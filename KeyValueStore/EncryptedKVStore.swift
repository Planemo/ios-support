//
//  CryptoStore.swift
//  KeyValueStore
//
//  Created by Mihail Shulepov on 29/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

/// Encrypts each value
open class EncryptedKVStore: KeyValueStoreEngine {
    public typealias CryptoFunc = (Data) -> Data!
    fileprivate let encryptor: CryptoFunc
    fileprivate let decryptor: CryptoFunc
    fileprivate let innerEngine: KeyValueStoreEngine
    
    public init(innerEngine: KeyValueStoreEngine, encryptor: @escaping CryptoFunc, decryptor: @escaping CryptoFunc) {
        self.encryptor = encryptor
        self.decryptor = decryptor
        self.innerEngine = innerEngine
    }
    
    open func setInteger(_ value: Int, forKey key: String) {
        storeString("\(value)", forKey: key)
    }
    open func integerForKey(_ key: String) -> Int? {
        if let decoded = stringForKey(key) {
            return Int(decoded)
        }
        return nil
    }
    
    open func setFloat(_ value: Float, forKey key: String) {
        storeString("\(value)", forKey: key)
    }
    open func floatForKey(_ key: String) -> Float? {
        if let decoded = stringForKey(key) {
            return Float(decoded)
        }
        return nil
    }
    
    open func setBool(_ value: Bool, forKey key: String) {
        let str = value ? "true" : "false"
        storeString(str, forKey: key)
    }
    open func boolForKey(_ key: String) -> Bool? {
        if let decoded = stringForKey(key) {
            if decoded == "true" {
                return true
            } else if decoded == "false" {
                return false
            }
        }
        return nil
    }
    
    open func setString(_ value: String, forKey key: String) {
        storeString(value, forKey: key)
    }
    
    open func stringForKey(_ key: String) -> String? {
        if let data = dataForKey(key) {
            return NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String
        }
        return nil
    }
    
    open func setDate(_ value: Date, forKey key: String) {
        storeObject(value as NSDate, forKey: key)
    }
    
    open func dateForKey(_ key: String) -> Date? {
        let nsDate: NSDate? = objectForKey(key)
        return nsDate as Date?
    }
    
    open func setData(_ value: Data, forKey key: String) {
        if let encrypted = encryptor(value) {
            self.innerEngine.setData(encrypted, forKey: key)
        }
    }
    
    open func dataForKey(_ key: String) -> Data? {
        if let encrypted = self.innerEngine.dataForKey(key) {
            return decryptor(encrypted)
        }
        return nil
    }

    open func removeObjectForKey(_ key: String) {
        self.innerEngine.removeObjectForKey(key)
    }
    
    open func save() {
        self.innerEngine.save()
    }
    
    fileprivate func storeString(_ string: String, forKey key: String) {
        if let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            setData(data, forKey: key)
        }
    }
    
    fileprivate func storeObject(_ object: NSCoding, forKey key: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        setData(data, forKey: key)
    }
    
    fileprivate func objectForKey<T: AnyObject>(_ key: String) -> T? where T: NSCoding {
        if let data = dataForKey(key) {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? T
        }
        return nil
    }
}
