//
//  LocalizationSwitcher.swift
//  davinci
//
//  Created by Mihail Shulepov on 17/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
import UIKit

open class LocalizationSwitcher: NSObject {
    @IBInspectable
    open var storyboardName: String = "Main"
    
    public override init() {

    }
    
    @IBAction
    open func switchLanguage() {
        let languages = (Bundle.availableLanguages).sorted{$0 < $1}
        let currentLanguage = Bundle.language ?? "Base"
        var nextLanguage = languages.first!
        if let idx = languages.index(of: currentLanguage) {
            let nextIdx = (idx + 1) % languages.count
            nextLanguage = languages[nextIdx]
        }
        Bundle.language = nextLanguage
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        let initialVC = storyboard.instantiateInitialViewController()
        let appDelegate = UIApplication.shared.delegate!
        appDelegate.window!?.rootViewController = initialVC
    }
}
