//
//  IAProduct.swift
//  davinci
//
//  Created by Mihail Shulepov on 01/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import StoreKit
#if !NO_MODULES
import RMStore
import MBProgressHUD
import ReactiveSwift
import Result
#endif

public let SettingsRestorePurchasesId = "restore_inapp_purchases"

/// Listener for completed transactions
public protocol IAStorePaymentsObserver: class {
    func didInitiateProductPaymentProcess(_ product: SKProduct)
    func didFinishPaymentTransaction(_ transaction: SKPaymentTransaction, forProduct: SKProduct!)
}


/// Products Loader
private class SKProductsLoader {
    // successfully loaded products
    fileprivate var loadedProducts = Set<String>()
    
    // invalid products (product ids does not exists or not ready)
    fileprivate var invalidProducts = Set<String>()
        
    func requestProducts(_ products: [String]) -> SignalProducer<[SKProduct], NSError> {
        let validProducts = Set(products).subtracting(invalidProducts)
        let toLoad = validProducts.subtracting(loadedProducts)
        if toLoad.isEmpty {
            let skProducts = productsWithIdentifiers(products)
            return SignalProducer(value: skProducts)
        } else {
            let producer = SignalProducer<[SKProduct], NSError> { [weak self] (observer, _) in
                //products load success block
                let loadSuccess = { (valid: [Any]?, invalidIdentifiers: [Any]?) -> Void in
                    if let strongSelf = self {
                        if let invalidIDs = invalidIdentifiers as? [String] {
                            strongSelf.invalidProducts.formUnion(Set(invalidIDs))
                        }
                        
                        if let valid = valid as? [SKProduct] {
                            let validIDs = valid.map { (product: SKProduct) -> String in
                                return product.productIdentifier
                            }
                            strongSelf.loadedProducts.formUnion(Set(validIDs))
                        }
                        
                        let requestedProducts = strongSelf.productsWithIdentifiers(products)
                        observer.send(value: requestedProducts)
                    }
                    observer.sendCompleted()
                }
                
                RMStore.default().requestProducts(toLoad,
                    success: loadSuccess,
                    failure: { error  in
                        observer.send(error: error as! NSError)
                    })
            }
            return producer
        }
    }
    
    fileprivate func productsWithIdentifiers(_ identifiers: [String]) -> [SKProduct] {
        return identifiers.map { identifier -> SKProduct? in
            return RMStore.default().product(forIdentifier: identifier)
        }.filter { $0 != nil }.map { $0!}
    }
}


/// Class only for reusable purchasing flow (UI + content providing)

open class IAStoreHelper: NSObject, RMStoreObserver {
    public struct Error {
        public static let Domain = "com.planemo.IAStore"
        public static let ProductLoadFailure = 1000
        public static let ProductNotDefined = 1001
        public static let PurchasesRestricted = 1002
    }
    
    open class var bundleMacro: String { return "{BUNDLE}" }
    
    fileprivate let receiptVerificator: RMStoreReceiptVerificator?
    
    internal var productInfos = [String: IAProductDefinition]()
    fileprivate var listeners = [IAStorePaymentsObserver]()
    fileprivate let productsLoader = SKProductsLoader()
    fileprivate let moduleBundle: Bundle
    
    open class var defaultHelper: IAStoreHelper {
        struct Static {
            static let instance: IAStoreHelper = IAStoreHelper()
        }
        return Static.instance
    }
    
    public override init() {
        if let clazz = NSClassFromString("RMStoreAppReceiptVerificator") as? NSObject.Type {
            self.receiptVerificator = clazz.init() as? RMStoreReceiptVerificator
        } else {
            self.receiptVerificator = nil
            NSLog("Using RMStore without receipt verification")
        }
        
        let selfBundle = Bundle(for: IAStoreHelper.self)
        if let resourcesBundlePath = selfBundle.path(forResource: "PLSupport", ofType: "bundle") {
            moduleBundle = Bundle(path: resourcesBundlePath) ?? selfBundle
        } else {
            moduleBundle = selfBundle
        }
        
        super.init()
        
        let rmstore = RMStore.default()
        rmstore?.add(self)
        rmstore?.receiptVerificator = self.receiptVerificator
    }
    
    /// Register new product with specified identifier (FULL identifier)
    /// Identifier may contain macro "{BUNDLE}" which will be replaced with actual bundle id
    open func registerProduct(_ identifier: String, info: IAProductDefinition) {
        let bundleID = Bundle.main.bundleIdentifier!
        let identifier = identifier.replacingOccurrences(of: IAStoreHelper.bundleMacro, with: bundleID,
            options: .caseInsensitive)
        self.productInfos[identifier] = info
    }
    
    
    // MARK: Queries for products
    
    /// Request products
    /// This method can be called multiple times - listeners will be added in queue
    open func requestProducts() -> SignalProducer<[IAProduct], NSError> {
        return self.requestProducts(Array(self.productInfos.keys))
    }
    
    /// Request specified products
    fileprivate func requestProducts(_ productIDs: [String]) -> SignalProducer<[IAProduct], NSError> {
        return self.productsLoader.requestProducts(productIDs).map { skProducts -> [IAProduct] in
            return skProducts.map { skProduct -> IAProduct in
                let definition = self.productInfos[skProduct.productIdentifier]!
                return IAProduct(definition: definition, skProduct: skProduct, store: self)
            }
        }
    }
    
    /// Load (if necessary) and return products
    open func products() -> SignalProducer<[IAProduct], NSError> {
        return self.requestProducts()
    }
    
    open func products(_ productIDs: [String]) -> SignalProducer<[IAProduct], NSError> {
        return self.requestProducts(productIDs)
    }
    
    /// Get product with identifier
    open func productWithIdentifier(_ identifier: String) -> SignalProducer<IAProduct, NSError> {
        if let fullProductID = self.fullProductIdentifierForIdentifier(identifier) {
            return self.requestProducts([fullProductID]).attemptMap { (products: [IAProduct]) -> Result<IAProduct, NSError> in
                if let product = products.first {
                    return Result.success(product)
                } else {
                    let userInfo = [NSLocalizedDescriptionKey: "Can't load product with id: \(identifier)"]
                    let error = NSError(domain: Error.Domain, code: Error.ProductLoadFailure, userInfo: userInfo)
                    return Result.failure(error)
                }
            }
        } else {
            let userInfo = [NSLocalizedDescriptionKey: "Product definition not found for: \(identifier)"]
            let error = NSError(domain: Error.Domain, code: Error.ProductNotDefined, userInfo: userInfo)
            return SignalProducer(error: error)
        }
    }
    
    
    // MARK: Purchasing and Restoring
    
    /// Purchase specified product
    /// Shows loading indicator
    /// Display error if product wasn't found
    /// Display dialog if payments restricted in phone settings
    open func purchaseProductWithIdentifier(_ identifier: String) -> SignalProducer<(), NSError> {
        return self.productWithIdentifier(identifier)
            .flatMap(.merge) { product in
                return self.purchaseProduct(product)
            }
    }
    
    open func purchaseProduct(_ product: IAProduct) -> SignalProducer<(), NSError> {
        if RMStore.canMakePayments() {
            return SignalProducer { observer, disposable in
                for listener in self.listeners {
                    listener.didInitiateProductPaymentProcess(product.skProduct)
                }
                
                RMStore.default().addPayment(product.productIdentifier, success: { transaction in
                    //delay required because content granted after this method being called
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        observer.sendCompleted()
                    }
                }, failure: { (transaction: SKPaymentTransaction?, error: Swift.Error?) -> Swift.Void in
                    if let error = error as? NSError {
                        observer.send(error: error)
                    } else {
                        let error = NSError(domain: "com.planemo.IAStore", code: 111, userInfo: nil)
                        observer.send(error: error)
                    }
                })
            }
        } else {
            UIAlertView(title: getLocalizedString("PurchasesDisabledTitle"),
                message: getLocalizedString("PurchasesDisabledMessage"),
                delegate: nil,
                cancelButtonTitle: nil,
                otherButtonTitles: getLocalizedString("OK"))
                .show()
            let userInfo = [NSLocalizedDescriptionKey: "Purchases restricted"]
            let error = NSError(domain: Error.Domain, code: Error.PurchasesRestricted, userInfo: userInfo)
            return SignalProducer(error: error)
        }
    }

    
    /// Request restore transactions
    /// All UI provided
    open func restoreTransactions() -> SignalProducer<[SKPaymentTransaction], NSError> {
        return SignalProducer { observer, disposable in
            RMStore.default().restoreTransactions(onSuccess: { restored in
                observer.send(value: restored as! [SKPaymentTransaction])
                observer.sendCompleted()
            }, failure: { error  in
                if let error = error as? NSError {
                    observer.send(error: error)
                } else {
                    let error = NSError(domain: "com.planemo.IAStore", code: 112, userInfo: nil)
                    observer.send(error: error)
                }
            })
        }
    }
    
   
    // MARK: Support
    
    /// Add Payments observer
    open func addPaymentsObserver(_ listener: IAStorePaymentsObserver) {
        self.listeners.append(listener)
    }
    
    /// Remove payments observer
    open func removePaymentsObserver(_ listener: IAStorePaymentsObserver) {
        for i in 0 ..< self.listeners.count {
            if self.listeners[i] === listener {
                self.listeners.remove(at: i)
                break
            }
        }
    }
    
    /// Find full identifier for requested identifier
    fileprivate func fullProductIdentifierForIdentifier(_ identifier: String) -> String? {
        let bundleID = Bundle.main.bundleIdentifier!
        
        // check for bundle macro
        let productIDs = self.productInfos.keys
        let bundleMacroResult = identifier.replacingOccurrences(of: IAStoreHelper.bundleMacro, with: bundleID)
        if productIDs.contains(bundleMacroResult) {
            return bundleMacroResult
        }
        
        // try append bundle id
        let bundleAppendResult = "\(bundleID).\(identifier)"
        if productIDs.contains(bundleAppendResult) {
            return bundleAppendResult
        }
        
        // find best option
        let possibleIdentifiers = productIDs.filter{ $0.hasSuffix(identifier) }
        if possibleIdentifiers.count == 1 {
            return possibleIdentifiers.first
        } else if possibleIdentifiers.count > 1 {
            NSLog("Too many products ends with %@", identifier)
        }
        
        return nil
    }
    
    fileprivate func containsProductDefinitionForIdentifier(_ identifier: String) -> Bool {
        return self.fullProductIdentifierForIdentifier(identifier) != nil
    }
    
    /// MARK: RMStore observer
    
    open func storePaymentTransactionFailed(_ notification: Notification!) {

    }
    
    open func storePaymentTransactionFinished(_ notification: Notification) {
        let productID = (notification as NSNotification).rm_productIdentifier
        let transaction = (notification as NSNotification).rm_transaction
        
        if let purchaseInfo = self.productInfos[productID!] {
            purchaseInfo.provideContentAction(purchaseInfo)
            
        } else {
            NSLog("Definition not found for product \(productID)")
        }
        
        self.productWithIdentifier(productID!).startWithResult { (result: Result<IAProduct, NSError>) in
            switch result {
            case .success(let product):
                for listener in self.listeners {
                    listener.didFinishPaymentTransaction(transaction!, forProduct: product.skProduct)
                }
            case .failure(_):
                break
            }
        }
    }
    
    internal func getLocalizedString(_ key: String) -> String {
        let ret = self.moduleBundle.localizedString(forKey: key, value: nil, table: "IAStore")
        print("\(key) -> \(ret)")
        return ret
    }
}

