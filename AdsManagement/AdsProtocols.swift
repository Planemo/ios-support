//
//  AdsProtocols.swift
//  test
//
//  Created by Mikhail Shulepov on 02/04/15.
//  Copyright (c) 2015 planemo. All rights reserved.
//

import UIKit

public protocol IInterstitialScheduler: NSObjectProtocol {
    weak var viewController: UIViewController? { get set }
       
    /// How often ads may be displayed (in seconds)
    var minIntervalBetweenAds: TimeUnit { get set }
   
    var minSecondsBetweenAds: TimeInterval { get set }
    
    /// When ads should fire (for example, fire every 5 loses)
    var eventsCountToFire: Int { get set }
    
    var adUnitID: String { get set }
    
    func fireEvent() -> Bool
    func showIfShould()
    func forceShow()
}

public protocol IBannerHolder: NSObjectProtocol {
    weak var viewController: UIViewController! { get set }
    var bannerID: String { get set }
    var hidden: Bool { get set }
}

public protocol IRewardedVideoAds: NSObjectProtocol {
    var location: String { get }
    
    var hasVideo: Bool { get }
    func showVideo(_ reward: (Int) -> Void)
    func cacheVideo()
}
