//
//  rc64.swift
//  davinciquiz
//
//  Created by Mihail Shulepov on 30/08/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

public struct RC4 {
    fileprivate static let BYTE_MAX = Int(UInt8.max)
    fileprivate static let BYTE_COUNT = BYTE_MAX + 1
    
    
    fileprivate class Key {
        var state = Array(repeating: UInt8(0), count: RC4.BYTE_COUNT)
        var x: UInt8 = 0
        var y: UInt8 = 0
    }
    
    fileprivate var key = Key()
    
    public init(key: String) {
        setupKey(key)
    }
    
    fileprivate func swap(_ array: inout [UInt8], lhs: Int, rhs: Int) {
        let temp = array[lhs]
        array[lhs] = array[rhs]
        array[rhs] = temp
    }
    
    fileprivate func setupKey(_ keyStr: String) {
        var buf = [UInt8]()
        for ch in keyStr.utf8 {
            buf.append(ch)
        }
        
        for i in 0...RC4.BYTE_MAX {
            self.key.state[i] = UInt8(i)
        }
        
        self.key.x = 0
        self.key.y = 0
        
        let len = buf.count
        var j = 0
        for i in 0...RC4.BYTE_MAX {
            j = (j + Int(self.key.state[i]) + Int(buf[i % len])) % RC4.BYTE_COUNT
            self.swap(&self.key.state, lhs: i, rhs: Int(j))
        }
    }
    
    public func crypt(_ data: Data) -> Data {
        let len = data.count
        let ptr = UnsafeMutablePointer<UInt8>(mutating: (data as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.count))
        let bytes = UnsafeMutableBufferPointer<UInt8>(start:ptr, count:data.count)
        
        var newBytes = Array(repeating: UInt8(0), count: len)
        
        var x = self.key.x
        var y = self.key.y
        for i in 0...len-1 {
            x = x &+ 1
            y = y &+ self.key.state[Int(x)]
            self.swap(&self.key.state, lhs: Int(x), rhs: Int(y))
            let idx = Int(self.key.state[Int(x)] &+ self.key.state[Int(y)])
            newBytes[i] = bytes[i] ^ self.key.state[idx]
        }
        
        self.key.x = x
        self.key.y = y
        
        return Data(bytes: UnsafePointer<UInt8>(newBytes), count: newBytes.count)
    }
    
    public static func asFunc(_ key: String) -> (Data) -> Data {
        return { data -> Data in
            return RC4(key: key).crypt(data)
        }
    }
    
}
