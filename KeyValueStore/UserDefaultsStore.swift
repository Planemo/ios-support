//
//  UserDefaultsStore.swift
//  KeyValueStore
//
//  Created by Mihail Shulepov on 29/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

open class UserDefaultsStore: KeyValueStoreEngine {
    open let userDefaults: UserDefaults
    
    public init(suiteName: String) {
        self.userDefaults = UserDefaults(suiteName: suiteName)!
    }
    
    public init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    open func setInteger(_ value: Int, forKey key: String) {
        self.userDefaults.set(value, forKey: key)
    }
    open func integerForKey(_ key: String) -> Int? {
        return self.userDefaults.object(forKey: key) as? Int
    }
    
    open func setFloat(_ value: Float, forKey key: String) {
        self.userDefaults.set(value, forKey: key)
    }
    open func floatForKey(_ key: String) -> Float? {
        return self.userDefaults.object(forKey: key) as? Float
    }
    
    open func setBool(_ value: Bool, forKey key: String) {
        self.userDefaults.set(value, forKey: key)
    }
    open func boolForKey(_ key: String) -> Bool? {
        return self.userDefaults.object(forKey: key) as? Bool
    }
    
    open func setString(_ value: String, forKey key: String) {
        self.userDefaults.set(value, forKey: key)
    }
    open func stringForKey(_ key: String) -> String? {
        return self.userDefaults.string(forKey: key)
    }
    
    open func setDate(_ value: Date, forKey key: String) {
        self.userDefaults.set(value, forKey: key)
    }
    open func dateForKey(_ key: String) -> Date? {
        return self.userDefaults.object(forKey: key) as? Date
    }
    
    open func setData(_ value: Data, forKey key: String) {
        self.userDefaults.set(value, forKey: key)
    }
    open func dataForKey(_ key: String) -> Data? {
        return self.userDefaults.data(forKey: key)
    }
    
    open func removeObjectForKey(_ key: String) {
        self.userDefaults.removeObject(forKey: key)
        self.userDefaults.synchronize()
    }
    
    open func save() {
        self.userDefaults.synchronize()
    }
}
