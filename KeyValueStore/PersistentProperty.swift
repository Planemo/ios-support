//
//  KeyValueStoreProperty.swift
//  test
//
//  Created by Mikhail Shulepov on 31/03/15.
//  Copyright (c) 2015 planemo. All rights reserved.
//

import Foundation
#if !NO_MODULES
import ReactiveSwift
import enum Result.NoError
#endif

private enum KVValueType {
    case integer
    case float
    case bool
    case string
    case date
    case data
    case object
}

open class PersistentProperty<T>: MutablePropertyProtocol {
    public typealias Value = T
    
    private let token: Lifetime.Token
    public let lifetime: Lifetime
    
    private let observer: Signal<T, NoError>.Observer
    
    fileprivate let storage: KeyValueStore
    fileprivate let key: String
    fileprivate let defaultValue: T
    fileprivate let valueType: KVValueType
    
    
    /// The current value of the property.
    ///
    /// Setting this to a new value will notify all observers of any Signals
    /// created from the `values` producer.
    open var value: T {
        get {
            var ret: T?
            switch self.valueType {
            case .integer:
                ret = self.storage.integerForKey(self.key) as? T
            case .float:
                ret = self.storage.floatForKey(self.key) as? T
            case .bool:
                ret = self.storage.boolForKey(self.key) as? T
            case .string:
                ret = self.storage.stringForKey(self.key) as? T
            case .date:
                ret = self.storage.dateForKey(self.key) as? T
            case .data:
                ret = self.storage.dataForKey(self.key) as? T
            case .object:
                ret = self.storage.objectForKey(self.key)
            }
            return ret ?? self.defaultValue
        }
        
        set(x) {
            var shouldRemove = false
            switch self.valueType {
            case .integer:
                if let intValue = x as? Int {
                    self.storage.setInteger(intValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .float:
                if let floatValue = x as? Float {
                    self.storage.setFloat(floatValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .bool:
                if let boolValue = x as? Bool {
                    self.storage.setBool(boolValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .string:
                if let stringValue = x as? String {
                    self.storage.setString(stringValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .date:
                if let dateValue = x as? Date {
                    self.storage.setDate(dateValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .data:
                if let dataValue = x as? Data {
                    self.storage.setData(dataValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .object:
                if let object = x as? NSCoding {
                    self.storage.setObject(object, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            }
            
            if shouldRemove {
                self.storage.removeObjectForKey(self.key)
            }
            
            observer.send(value: x)
        }
    }
    
    public let signal: Signal<T, NoError>
    
    /// A producer for Signals that will send the property's current value,
    /// followed by all changes over time, then complete when the property has
    /// deinitialized.
    public var producer: SignalProducer<Value, NoError> {
        return SignalProducer { [weak self] producerObserver, producerDisposable in
                if let strongSelf = self {
                    producerObserver.send(value: strongSelf.value)
                    producerDisposable += strongSelf.signal.observe(producerObserver)
                } else {
                    producerObserver.sendCompleted()
                }
        }
    }
    
    /// Initializes the property with the given value to start.
    private init(storage: KeyValueStore, key: String, defaultValue: T, type: KVValueType) {
        self.token = Lifetime.Token()
        self.lifetime = Lifetime(token)
        
        self.storage = storage
        self.key = key
        self.valueType = type
        
        let (signal, observer) = Signal<T, NoError>.pipe()
        self.signal = signal
        self.observer = observer
        
        self.defaultValue = defaultValue
        self.observer.send(value: self.value)
    }
    
    open class func bool(storage: KeyValueStore, key: String, defaultValue: Bool = false) -> PersistentProperty<Bool> {
        return PersistentProperty<Bool>(storage: storage, key: key, defaultValue: defaultValue, type: .bool)
    }
    
    open class func int(storage: KeyValueStore, key: String, defaultValue: Int = 0) -> PersistentProperty<Int> {
        return PersistentProperty<Int>(storage: storage, key: key, defaultValue: defaultValue, type: .integer)
    }
    
    open class func float(storage: KeyValueStore, key: String, defaultValue: Float = 0) -> PersistentProperty<Float> {
        return PersistentProperty<Float>(storage: storage, key: key, defaultValue: defaultValue, type: .float)
    }
    
    open class func string(storage: KeyValueStore, key: String, defaultValue: String) -> PersistentProperty<String> {
        return PersistentProperty<String>(storage: storage, key: key, defaultValue: defaultValue, type: .string)
    }
    
    open class func date(storage: KeyValueStore, key: String, defaultValue: Date) -> PersistentProperty<Date> {
        return PersistentProperty<Date>(storage: storage, key: key, defaultValue: defaultValue, type: .date)
    }
    
    open class func data(storage: KeyValueStore, key: String, defaultValue: Data) -> PersistentProperty<Data> {
        return PersistentProperty<Data>(storage: storage, key: key, defaultValue: defaultValue, type: .data)
    }
    
    open class func object<U: AnyObject>(storage: KeyValueStore, key: String, defaultValue: U) -> PersistentProperty<U> where U: NSCoding {
        return PersistentProperty<U>(storage: storage, key: key, defaultValue: defaultValue, type: .object)
    }
    
    open class func array<U: AnyObject>(storage: KeyValueStore, key: String) -> PersistentProperty<[U]> where U: NSCoding {
        return PersistentProperty<[U]>(storage: storage, key: key, defaultValue: [], type: .object)
    }
    
    deinit {
        observer.sendCompleted()
    }
}

open class OptionalPersistentProperty<T>: MutablePropertyProtocol {
    public typealias Value = T?
    
    private let token: Lifetime.Token
    public let lifetime: Lifetime
    
    fileprivate let observer: Signal<T?, NoError>.Observer
    
    fileprivate let storage: KeyValueStore
    fileprivate let key: String
    fileprivate let valueType: KVValueType
    
    
    /// The current value of the property.
    ///
    /// Setting this to a new value will notify all observers of any Signals
    /// created from the `values` producer.
    open var value: T? {
        get {
            var ret: T?
            switch self.valueType {
            case .integer:
                ret = self.storage.integerForKey(self.key) as? T
            case .float:
                ret = self.storage.floatForKey(self.key) as? T
            case .bool:
                ret = self.storage.boolForKey(self.key) as? T
            case .string:
                ret = self.storage.stringForKey(self.key) as? T
            case .date:
                ret = self.storage.dateForKey(self.key) as? T
            case .data:
                ret = self.storage.dataForKey(self.key) as? T
            case .object:
                ret = self.storage.objectForKey(self.key)
            }
            return ret
        }
        
        set(x) {
            var shouldRemove = false
            switch self.valueType {
            case .integer:
                if let intValue = x as? Int {
                    self.storage.setInteger(intValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .float:
                if let floatValue = x as? Float {
                    self.storage.setFloat(floatValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .bool:
                if let boolValue = x as? Bool {
                    self.storage.setBool(boolValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .string:
                if let stringValue = x as? String {
                    self.storage.setString(stringValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .date:
                if let dateValue = x as? Date {
                    self.storage.setDate(dateValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .data:
                if let dataValue = x as? Data {
                    self.storage.setData(dataValue, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            case .object:
                if let object = x as? NSCoding {
                    self.storage.setObject(object, forKey: self.key)
                } else {
                    shouldRemove = true
                }
            }
            
            if shouldRemove {
                self.storage.removeObjectForKey(self.key)
            }
            
            observer.send(value: x)
        }
    }
    
    public let signal: Signal<T?, NoError>
    
    /// A producer for Signals that will send the property's current value,
    /// followed by all changes over time, then complete when the property has
    /// deinitialized.
    public var producer: SignalProducer<T?, NoError> {
        return SignalProducer { [weak self] producerObserver, producerDisposable in
            if let strongSelf = self {
                producerObserver.send(value: strongSelf.value)
                producerDisposable += strongSelf.signal.observe(producerObserver)
            } else {
                producerObserver.sendCompleted()
            }
        }
    }
    
    /// Initializes the property with the given value to start.
    fileprivate init(storage: KeyValueStore, key: String, type: KVValueType) {
        self.token = Lifetime.Token()
        self.lifetime = Lifetime(token)
        
        self.storage = storage
        self.key = key
        self.valueType = type
        
        let (signal, observer) = Signal<T?, NoError>.pipe()
        self.signal = signal
        self.observer = observer
        
        self.observer.send(value: self.value)
    }
    
    open class func bool(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<Bool> {
        return OptionalPersistentProperty<Bool>(storage: storage, key: key, type: .bool)
    }
    
    open class func int(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<Int> {
        return OptionalPersistentProperty<Int>(storage: storage, key: key, type: .integer)
    }
    
    open class func float(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<Float> {
        return OptionalPersistentProperty<Float>(storage: storage, key: key, type: .float)
    }
    
    open class func string(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<String> {
        return OptionalPersistentProperty<String>(storage: storage, key: key, type: .string)
    }
    
    open class func date(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<Date> {
        return OptionalPersistentProperty<Date>(storage: storage, key: key, type: .date)
    }
    
    open class func data(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<Data> {
        return OptionalPersistentProperty<Data>(storage: storage, key: key, type: .data)
    }
    
    open class func object<U: AnyObject>(storage: KeyValueStore, key: String) -> OptionalPersistentProperty<U> where U: NSCoding {
        return OptionalPersistentProperty<U>(storage: storage, key: key, type: .object)
    }
    
    deinit {
        observer.sendCompleted()
    }
}
