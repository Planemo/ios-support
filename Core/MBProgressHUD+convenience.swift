//
//  MBProgressHUD+convenience.swift
//  test
//
//  Created by Mikhail Shulepov on 04.12.15.
//  Copyright © 2015 planemo. All rights reserved.
//

import Foundation
import MBProgressHUD

internal class MBProgressHUDDummyClass {}

public extension MBProgressHUD {
    
    fileprivate static var successImage: UIImage!
    fileprivate static var errorImage: UIImage!
    fileprivate static var infoImage: UIImage!
    
    fileprivate static func setupImages() {
        let bundle = Bundle(for: MBProgressHUDDummyClass.self)
        let url = bundle.url(forResource: "PLSupport", withExtension: "bundle")!
        let imagesBundle = Bundle(url: url)!
        
        infoImage = UIImage(contentsOfFile: imagesBundle.path(forResource: "info", ofType: "png")!)!
            .withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        errorImage = UIImage(contentsOfFile: imagesBundle.path(forResource: "error", ofType: "png")!)!
            .withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        successImage = UIImage(contentsOfFile: imagesBundle.path(forResource: "success", ofType: "png")!)!
            .withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }
    
    fileprivate static var topWindow: UIWindow {
        let frontToBackWindows = UIApplication.shared.windows.reversed()
        for window in frontToBackWindows {
            let windowOnMainScreen = window.screen == UIScreen.main;
            let windowIsVisible = !window.isHidden && window.alpha > 0;
            let windowLevelNormal = window.windowLevel == UIWindowLevelNormal;
            
            if windowOnMainScreen && windowIsVisible && windowLevelNormal {
                return window
            }
        }
        return frontToBackWindows.first!
    }
    
    public static func showGlobalHUD() -> MBProgressHUD {
        return self.showAdded(to: self.topWindow, animated: true)
    }
    
    public static func showHUDAddedTo(_ view: UIView, withError status: String) {
        let hud = self.showAdded(to: view, animated: true)
        hud.hideWithErrorStatus(status)
    }
    
    public static func showHUDAddedTo(_ view: UIView, withSuccess status: String) {
        let hud = self.showAdded(to: view, animated: true)
        hud.hideWithSuccessStatus(status)
    }
    
    public static func showHUDAddedTo(_ view: UIView, withInfo status: String) {
        let hud = self.showAdded(to: view, animated: true)
        hud.hideWithInfoStatus(status)
    }
    
    public func hideWithErrorStatus(_ status: String) {
        self.setErrorStatus(status)
        self.hideWithStatus(status)
    }
    
    public func hideWithSuccessStatus(_ status: String) {
        self.setSuccessStatus(status)
        self.hideWithStatus(status)
    }
    
    public func hideWithInfoStatus(_ status: String) {
        self.setInfoStatus(status)
        self.hideWithStatus(status)
    }
    
    public func setErrorStatus(_ status: String) {
        MBProgressHUD.setupImages()
        self.mode = MBProgressHUDMode.customView
        self.customView = UIImageView(image: MBProgressHUD.errorImage)
        self.customView?.tintColor = self.label.textColor
        self.label.text = status
    }
    
    public func setSuccessStatus(_ status: String) {
        MBProgressHUD.setupImages()
        self.mode = MBProgressHUDMode.customView
        self.customView = UIImageView(image: MBProgressHUD.successImage)
        self.customView?.tintColor = self.label.textColor
        self.label.text = status
    }
    
    public func setInfoStatus(_ status: String) {
        MBProgressHUD.setupImages()
        self.mode = MBProgressHUDMode.customView
        self.customView = UIImageView(image: MBProgressHUD.infoImage)
        self.customView?.tintColor = self.label.textColor
        self.label.text = status
    }
    
    fileprivate func hideWithStatus(_ status: String) {
        self.hide(animated: true, afterDelay: self.displayDurationForString(status))
    }
    
    fileprivate func displayDurationForString(_ string: String) -> TimeInterval {
        let delayValue = 0.5 + Double(string.utf16.count) * 0.06
        return min(delayValue, 5.0)
    }
}
