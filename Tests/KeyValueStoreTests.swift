//
//  KeyValueStoreTests.swift
//  KeyValueStoreTests
//
//  Created by Mihail Shulepov on 29/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import XCTest

class SerializableClass: NSObject, NSCoding, Equatable {
    let intValue = random()
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.intValue = aDecoder.decodeIntegerForKey("IntValue")
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInteger(self.intValue, forKey: "IntValue")
    }
}

func ==(lhs: SerializableClass, rhs: SerializableClass) -> Bool {
    return lhs.intValue == rhs.intValue
}

class KeyValueStoreTests: XCTestCase {
    
    private struct Key {
        static let KBoolean = "PLBoolean"
        static let KInteger = "PLInteger"
        static let KDate = "PLDate"
        static let KFloat = "PLFloat"
        static let KString = "PLString"
        static let KObject = "PLObject"
        static let KArray = "PLArray"
        static let KUrl = "PLUrl"
        static let KRemoving = "PLRemoving"
        
        static var allKeys: [String] {
            return [KBoolean, KInteger, KDate, KFloat, KString, KObject, KArray, KUrl]
        }
    }
    
    private var intValue: Int = random()
    private var floatValue: Float = Float(random())
    private var boolValue: Bool = (random() % 2) == 0
    private var stringValue: String = "sample string"
    private var dateValue: NSDate = NSDate(timeIntervalSince1970: NSTimeInterval(random()))
    private var urlValue: NSURL = NSURL(string: "http://google.com")!
    private var objectValue = SerializableClass()
    
    private var documentsDirectory: String {
        return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as String
    }
    
    override func setUp() {
        super.setUp()
        
        let seed = UInt32(NSDate().timeIntervalSinceReferenceDate % NSTimeInterval(UInt32.max))
        srandom(seed)
        intValue = random()
        floatValue = (Float(random()) / Float(Int.max)) * 1000
        boolValue = (random() % 2) == 0
        stringValue = "sample string"
        dateValue = NSDate(timeIntervalSince1970: NSTimeInterval(random()))
        urlValue = NSURL(string: "http://google.com")!
        objectValue = SerializableClass()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testUserDefaultsStore() {
        let ud = NSUserDefaults(suiteName: "SomeOtherTests")
        //checkKeyValueEngine(UserDefaultsStore(userDefaults: NSUserDefaults.standardUserDefaults()))
    }
    
    func testFilesystemStore() {
        let filePath = self.documentsDirectory.stringByAppendingPathComponent("fs.pl")

        let engine = FilesystemKVStore(filePath: filePath)
        checkKeyValueEngine(engine)
        engine.save()
        
        let reloadedEngine = FilesystemKVStore(filePath: filePath)
        checkPersistance(reloadedEngine)
        
        NSFileManager.defaultManager().removeItemAtPath(filePath, error: nil)
    }
    
    func testEncryptedFilesystemStore() {
        let filePath = self.documentsDirectory.stringByAppendingPathComponent("enfs.pl")
        NSFileManager.defaultManager().removeItemAtPath(filePath, error: nil)
        
        let crypter = RC4.asFunc("testkedsf sdfsf sdsy123")
        let engine = EncryptedFilesystemKVStore(filePath: filePath, encryptor: crypter, decryptor: crypter)
        checkKeyValueEngine(engine)
        engine.save()
        
        let reloadedEngine = EncryptedFilesystemKVStore(filePath: filePath, encryptor: crypter, decryptor: crypter)
        checkPersistance(reloadedEngine)
        
        NSFileManager.defaultManager().removeItemAtPath(filePath, error: nil)
    }
    
    func testEncryptedStore() {
        let filePath = self.documentsDirectory.stringByAppendingPathComponent("enffs.pl")
        NSFileManager.defaultManager().removeItemAtPath(filePath, error: nil)
        
        let crypter = RC4.asFunc("testkey123")
        let baseEngine = FilesystemKVStore(filePath: filePath)
        let engine = EncryptedKVStore(innerEngine: baseEngine, encryptor: crypter, decryptor: crypter)
        checkKeyValueEngine(engine)
        engine.save()
        
        let reloadedEngine = EncryptedKVStore(innerEngine: baseEngine, encryptor: crypter, decryptor: crypter)
        checkPersistance(reloadedEngine)
        
        NSFileManager.defaultManager().removeItemAtPath(filePath, error: nil)
    }
    
    func checkKeyValueEngine(engine: KeyValueStoreEngine) {
        let store = KeyValueStore(engine: engine)
        checkDateStoring(store)
        checkIntegerStoring(store)
        checkFloatStoring(store)
        checkBooleanStoring(store)
        checkStringStoring(store)
        checkCustomObjectStoring(store)
        checkUrlStoring(store)
        checkArrayStoring(store)
        checkRemoving(store)
    }
    
    func checkDateStoring(store: KeyValueStore) {
        let date = self.dateValue
        
        // test returning default value
        let providedDefaultValue = date.dateByAddingTimeInterval(100)
        let defaultValue = store.dateForKey(Key.KDate, defaultValue: providedDefaultValue)
        let defaultValueEquals = defaultValue.compare(providedDefaultValue) == .OrderedSame
        XCTAssertTrue(defaultValueEquals, "Should return provided default value")
        
        // test storing / retrieving
        store.setDate(date, forKey: Key.KDate)
        var equals = false
        if let restoredDate = store.dateForKey(Key.KDate) {
            if date.compare(restoredDate) == NSComparisonResult.OrderedSame {
                equals = true
            }
        }
        XCTAssertTrue(equals, "Stored and restored dates should be equal")
    }
    
    func checkIntegerStoring(store: KeyValueStore) {
        let value = self.intValue
        
        // test returning default value
        let providedDefaultValue = value + 100
        let defaultValue = store.integerForKey(Key.KInteger, defaultValue: providedDefaultValue)
        let defaultValueEquals = defaultValue == providedDefaultValue
        XCTAssertTrue(defaultValueEquals, "Should return provided default value")
        
        // test storing / retrieving
        store.setInteger(value, forKey: Key.KInteger)
        var equals = false
        if let restoredValue = store.integerForKey(Key.KInteger) {
            equals = value == restoredValue
        }
        XCTAssertTrue(equals, "Stored and restored values should be equal")
    }
    
    func checkFloatStoring(store: KeyValueStore) {
        let value = self.floatValue
        
        // test returning default value
        let providedDefaultValue = value + 100
        let defaultValue = store.floatForKey(Key.KFloat, defaultValue: providedDefaultValue)
        let defaultValueEquals = defaultValue == providedDefaultValue
        XCTAssertTrue(defaultValueEquals, "Should return provided default value")
        
        // test storing / retrieving
        store.setFloat(value, forKey: Key.KFloat)
        var equals = false
        if let restoredValue = store.floatForKey(Key.KFloat) {
            equals = abs(value - restoredValue) < 0.01
        }
        XCTAssertTrue(equals, "Stored and restored values should be equal")
    }
    
    func checkBooleanStoring(store: KeyValueStore) {
        let value = self.boolValue
        
        // test returning default value
        let providedDefaultValue = !value
        let defaultValue = store.boolForKey(Key.KBoolean, defaultValue: providedDefaultValue)
        let defaultValueEquals = defaultValue == providedDefaultValue
        XCTAssertTrue(defaultValueEquals, "Should return provided default value")
        
        // test storing / retrieving
        store.setBool(value, forKey: Key.KBoolean)
        var equals = false
        if let restoredValue = store.boolForKey(Key.KBoolean) {
            equals = value == restoredValue
        }
        XCTAssertTrue(equals, "Stored and restored values should be equal")
    }
    
    func checkStringStoring(store: KeyValueStore) {
        let value = self.stringValue
        
        // test returning default value
        let providedDefaultValue = "provided default string"
        let defaultValue = store.stringForKey(Key.KString, defaultValue: providedDefaultValue)
        let defaultValueEquals = defaultValue == providedDefaultValue
        XCTAssertTrue(defaultValueEquals, "Should return provided default value")
        
        // test storing / retrieving
        store.setString(value, forKey: Key.KString)
        var equals = false
        if let restoredValue = store.stringForKey(Key.KString) {
            equals = value == restoredValue
        }
        XCTAssertTrue(equals, "Stored and restored values should be equal")
    }
    
    func checkCustomObjectStoring(store: KeyValueStore) {
        let value = self.objectValue
        
        // test storing / retrieving
        store.setObject(value, forKey: Key.KObject)
        var equals = false
        if let restoredValue: SerializableClass = store.objectForKey(Key.KObject) {
            equals = value == restoredValue
        }
        XCTAssertTrue(equals, "Stored and restored objects should be equal")
    }
    
    func checkUrlStoring(store: KeyValueStore) {
        let value = self.urlValue
        
        // test storing / retrieving
        store.setURL(value, forKey: Key.KUrl)
        var equals = false
        if let restoredValue = store.urlForKey(Key.KUrl) {
            equals = value == restoredValue
        }
        XCTAssertTrue(equals, "Stored and restored values should be equal")
    }
    
    func checkArrayStoring(store: KeyValueStore) {
        //TODO:
    }
    
    func checkRemoving(store: KeyValueStore) {
        store.setInteger(self.intValue, forKey: Key.KRemoving)
        XCTAssert(store.integerForKey(Key.KRemoving) != nil, "Value should exist")
        store.removeObjectForKey(Key.KRemoving)
        XCTAssert(store.integerForKey(Key.KRemoving) == nil, "Value should not exist")
    }
    
    func checkPersistance(engine: KeyValueStoreEngine) {
        let store = KeyValueStore(engine: engine)
        
        var intEquals = false
        var floatEquals = false
        var stringEquals = false
        var boolEquals = false
        var urlEquals = false
        var customObjectEquals = false
        var arrayEquals = false
        
        if let value = store.integerForKey(Key.KInteger) {
            intEquals = value == self.intValue
        }
        XCTAssertTrue(intEquals, "Values should persist")
        
        if let value = store.floatForKey(Key.KFloat) {
            floatEquals = abs(value - self.floatValue) < 0.01
        }
        XCTAssertTrue(floatEquals, "Values should persist")
        
        if let value = store.stringForKey(Key.KString) {
            stringEquals = value == self.stringValue
        }
        XCTAssertTrue(stringEquals, "Values should persist")
        
        if let value = store.boolForKey(Key.KBoolean) {
            boolEquals = value == self.boolValue
        }
        XCTAssertTrue(boolEquals, "Values should persist")
        
        if let value = store.urlForKey(Key.KUrl) {
            urlEquals = value == self.urlValue
        }
        XCTAssertTrue(urlEquals, "Values should persist")

        if let value: SerializableClass = store.objectForKey(Key.KObject) {
            customObjectEquals = value == self.objectValue
        }
        XCTAssertTrue(urlEquals, "Objects should persist")
        
    }
}
