//
//  IAStoreHelper+ReceiptVerifier.swift
//  davinci
//
//  Created by Mihail Shulepov on 17/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
#if !NO_MODULES
import RMStore
#endif

public extension IAStoreHelper {
    /// Returns whether app receipt already contains within app
    private func containsAppReceipt() -> Bool {
        let fm = NSFileManager.defaultManager()
        if let receiptPath = NSBundle.mainBundle().appStoreReceiptURL?.path {
            return fm.fileExistsAtPath(receiptPath)
        }
        return false
    }
    
    /// Refresh app receipt and validate whether receipt contains products for all provided content
    public func refreshAppReceiptAndSyncProvidedContent() {
        if !containsAppReceipt() {
            return
        }
        RMStore.defaultStore().refreshReceiptOnSuccess({ [weak self] in
            if let strongSelf = self {
                strongSelf.syncProvidedContentWithAppReceiptProducts()
            }
        }, failure: { (error: NSError!) -> Void in
            
        })
    }
    
    /// If product.contentProvided != receipt.containsProduct then provide required content or remove it
    public func syncProvidedContentWithAppReceiptProducts() {
        if let receipt = RMAppReceipt.bundleReceipt() {
            let allProducts = self.productInfos
            for (identifier, productInfo) in allProducts {
                /// Take into account only NonConsumable products
                if productInfo.type == .NonConsumable {
                    let containsProduct = receipt.containsInAppPurchaseOfProductIdentifier(identifier)
                    let contentProvided = productInfo.contentProvided()
                    switch (contentProvided, containsProduct) {
                    case (true,false): productInfo.removeContentAction?(productInfo)
                    case (false,true): productInfo.provideContentAction(productInfo)
                    default: break
                    }
                }
            }
        }
    }
}
