//
//  RC4Tests.swift
//  KeyValueStore
//
//  Created by Mihail Shulepov on 11/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import XCTest

class RC4Tests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEncode() {
        let key = "dfjkjkiu945j arioe43a0"
        let crypter = RC4(key: key)
        
        let testString = "Sample string with русские буквы"
        let data = testString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        let cryptedData = crypter.crypt(data!)
        XCTAssert(!cryptedData.isEqualToData(data!), "Pass")
    }
    
    func testEncodeDecode() {
        let key = "dfjkjkiu945j arioe43a0"
        let crypter = RC4(key: key)
        
        let testString = "Sample string with русские буквы:\n %˚˚…∆ƒ"
        let data = testString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!
        let cryptedData = crypter.crypt(data)
        
        let decrypter = RC4(key: key)
        let decryptedData = decrypter.crypt(cryptedData)
        let decryptedString: String = NSString(data: decryptedData, encoding: NSUTF8StringEncoding)!
        
        XCTAssert(testString == decryptedString, "Pass")
    }
    
    func testPerformance() {
        self.measureBlock() {
            let key = "dfjkjkiu945j arioe43a0"
            let crypter = RC4(key: key)
            
            let testString = "Sample string with русские буквы %˚˚\n…∆ƒ Sample string with русские буквы %˚˚…∆ƒ Sample string with русские буквы %˚˚…∆ƒ"
            let data = testString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!
            let cryptedData = crypter.crypt(data)
            
        }
    }


}
