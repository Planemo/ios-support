//
//  KeyValueStore.swift
//  KeyValueStore
//
//  Created by Mihail Shulepov on 29/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

public protocol KeyValueStoreEngine {
    func setInteger(_ value: Int, forKey: String)
    func integerForKey(_ key: String) -> Int?
    
    func setFloat(_ value: Float, forKey: String)
    func floatForKey(_ key: String) -> Float?
    
    func setBool(_ value: Bool, forKey: String)
    func boolForKey(_ key: String) -> Bool?

    func setString(_ value: String, forKey: String)
    func stringForKey(_ key: String) -> String?
    
    func setDate(_ value: Date, forKey: String)
    func dateForKey(_ key: String) -> Date?
    
    func setData(_ value: Data, forKey: String)
    func dataForKey(_ key: String) -> Data?
       
    func removeObjectForKey(_ key: String)
    
    func save()
}

