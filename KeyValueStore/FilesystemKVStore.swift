//
//  FilesystemKVStore.swift
//  KeyValueStore
//
//  Created by Mihail Shulepov on 29/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

/// Stored as file
open class FilesystemKVStore: KeyValueStoreEngine {
    fileprivate let queue = DispatchQueue(label: "FilesystemKVStore", attributes: [])
    fileprivate var dictionary: NSMutableDictionary!
    open let filePath: URL
    open var preventFromBackup = true
    fileprivate var loaded = false
    
    public init(filePath: String) {
        self.filePath = URL(fileURLWithPath: filePath)
        self.queue.async {
            self.dictionary = self.load() ?? NSMutableDictionary()
            self.loaded = true
        }
    }
    
    open func setInteger(_ value: Int, forKey key: String) {
        self.executeAfterLoaded {
            self.dictionary[key] = value
        }
    }
    open func integerForKey(_ key: String) -> Int? {
        self.waitForLoad()
        return self.dictionary[key] as? Int
    }
    
    open func setFloat(_ value: Float, forKey key: String) {
        self.executeAfterLoaded {
            self.dictionary[key] = value
        }
    }
    open func floatForKey(_ key: String) -> Float? {
        self.waitForLoad()
        return self.dictionary[key] as? Float
    }

    open func setBool(_ value: Bool, forKey key: String) {
        self.executeAfterLoaded {
            self.dictionary[key] = value
        }
    }
    open func boolForKey(_ key: String) -> Bool? {
        self.waitForLoad()
        return self.dictionary[key] as? Bool
    }
    
    open func setString(_ value: String, forKey key: String) {
        self.executeAfterLoaded {
            self.dictionary[key] = value
        }
    }
    open func stringForKey(_ key: String) -> String? {
        self.waitForLoad()
        return self.dictionary[key] as? String
    }
    
    open func setDate(_ value: Date, forKey key: String) {
        self.executeAfterLoaded {
            self.dictionary[key] = value
        }
    }
    open func dateForKey(_ key: String) -> Date? {
        self.waitForLoad()
        return self.dictionary[key] as? Date
    }
    
    open func setData(_ value: Data, forKey key: String) {
        self.executeAfterLoaded {
            self.dictionary[key] = value
        }
    }
    open func dataForKey(_ key: String) -> Data? {
        self.waitForLoad()
        return self.dictionary[key] as? Data
    }
    
    open func removeObjectForKey(_ key: String) {
        self.executeAfterLoaded {
            self.dictionary.removeObject(forKey: key); return
        }
    }
    
    fileprivate func waitForLoad() {
        if !self.loaded {
            self.queue.sync {
                
            }
        }
    }
    
    fileprivate func executeAfterLoaded(_ closure: @escaping () -> ()) {
        if self.loaded {
            closure()
        } else {
            self.queue.async(execute: closure)
        }
    }
    
    open func save() {
        self.queue.sync {
            let fileManager = FileManager.default
            let dirPath = self.filePath.deletingLastPathComponent()
            if !fileManager.fileExists(atPath: dirPath.absoluteString) {
                do {
                    try fileManager.createDirectory(at: dirPath, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    print("Error creating directories at path: \(dirPath),  \(error.description)")
                }
            }
            let data = self.asData()
            try? data.write(to: self.filePath, options: [.atomic])
            if self.preventFromBackup {
                do {
                    try (self.filePath as NSURL).setResourceValue(true, forKey: URLResourceKey.isExcludedFromBackupKey)
                } catch let error as NSError {
                    print("Error preventing from backup: \(error.description)")
                }
            }
        }
    }
    
    internal func load() -> NSMutableDictionary? {
        if let data = self.savedData() {
            let object: AnyObject? = NSKeyedUnarchiver.unarchiveObject(with: data) as AnyObject?
            if let dict = object as? NSDictionary {
                return dict.mutableCopy() as? NSMutableDictionary
            }
            // compatibility
            if let dict = object as? [String: AnyObject] {
                let ret = NSMutableDictionary()
                for (key, value) in dict {
                    ret[key] = value
                }
                return ret
            }
        }
        return nil
    }
    
    internal func asData() -> Data {
        return NSKeyedArchiver.archivedData(withRootObject: self.dictionary)
    }
    
    internal func savedData() -> Data? {
        return (try? Data(contentsOf: filePath))
    }
}

/// Encrypts whole file
open class EncryptedFilesystemKVStore: FilesystemKVStore {
    public typealias CryptoFunc = (Data) -> Data!
    
    fileprivate let encryptor: CryptoFunc
    fileprivate let decryptor: CryptoFunc
    
    public init(filePath: String, encryptor: @escaping CryptoFunc, decryptor: @escaping CryptoFunc) {
        self.encryptor = encryptor
        self.decryptor = decryptor
        super.init(filePath: filePath)
    }
    
    override internal func asData() -> Data {
        return encryptor(super.asData())
    }
    
    override internal func savedData() -> Data? {
        if let data = super.savedData() {
            return decryptor(data)
        }
        return nil
    }
}
