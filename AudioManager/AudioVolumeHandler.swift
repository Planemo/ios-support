//
//  AudioMuteButton.swift
//  davinci
//
//  Created by Mihail Shulepov on 06/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

open class AudioVolumeHandler: NSObject {
    public struct Configuration {
        public static var defaultMusicVolume: CGFloat = 1.0
        public static var defaultSoundsVolume: CGFloat = 1.0
    }
    
    @IBInspectable open var muteMusic: Bool = true
    @IBInspectable open var muteSounds: Bool = true

    @IBInspectable open var defaultSoundsVolume: CGFloat = AudioVolumeHandler.Configuration.defaultSoundsVolume
    @IBInspectable open var defaultMusicVolume: CGFloat = AudioVolumeHandler.Configuration.defaultMusicVolume
    
    @IBOutlet open var button: UIButton! {
        didSet {
            button.addTarget(self, action: #selector(AudioVolumeHandler.toggleMute), for: .touchUpInside)
            updateState()
        }
    }
    
    open func toggleMute() {
        let audioManager = AudioManager.defaultManager
        var shouldMute: Bool!
        if muteMusic {
            shouldMute = !audioManager.musicMuted
        } else if muteSounds {
            shouldMute = !audioManager.soundsMuted
        }
        
        if muteMusic {
            audioManager.musicVolume = Float(shouldMute! ? 0.0 : defaultMusicVolume)
        }
        if muteSounds {
            audioManager.soundsVolume = Float(shouldMute! ? 0.0 : defaultSoundsVolume)
        }
        updateState()
    }
    
    fileprivate func updateState() {
        if self.muteMusic {
            self.button.isSelected = AudioManager.defaultManager.musicMuted
        } else if self.muteSounds {
            self.button.isSelected = AudioManager.defaultManager.soundsMuted
        }
    }
}
