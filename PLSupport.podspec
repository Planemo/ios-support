Pod::Spec.new do |s|
  s.name         =  'PLSupport'
  s.version      =  '0.5.7'
  s.license      =  'MIT'
  s.summary      =  'Convenience wrapper for most frequently used things.'
  s.homepage     =  'https://bitbucket.org/Planemo/ios-support'
  s.authors      =  { 'Shulepov Mikhail' => 'shulepov.mikhail@gmail.com' }
  s.source       =  { :git => 'git@bitbucket.org:Planemo/ios-support.git' }
  s.ios.deployment_target = "8.0"
  s.requires_arc =  true
  s.summary      =  'Using subspecs you can define what things you exactly need.'
  s.module_name  = 'PLSupport'

  s.dependency 'ReactiveSwift', '~> 1.0.0-rc.1'

  s.subspec 'Core' do |ss|
    ss.source_files = 'Core/*'
    ss.resources = 'PLSupport.bundle'
    ss.dependency 'MBProgressHUD'
  end

  s.subspec 'Ads' do |ss|
    ss.dependency 'PLSupport/Core'
    ss.dependency 'Google-Mobile-Ads-SDK'
    ss.dependency 'ChartboostSDK'
    ss.source_files = 'AdsManagement/*.swift'
    ss.exclude_files = 'AdsManagement/AdsEmpty.swift', 'AdsManagement/AdsSettings.swift'
  end

  s.subspec 'AdsEmpty' do |ss|
    ss.dependency 'PLSupport/Core'
    ss.source_files = 'AdsManagement/AdsEmpty.swift', 'AdsManagement/AdsSettings.swift', 'AdsManagement/AdsProtocols.swift'
  end

  s.subspec 'Audio' do |ss|
    ss.dependency 'PLSupport/Core'
    ss.source_files = 'AudioManager/*.swift'
    ss.framework = 'AVFoundation'
  end

  s.subspec 'CrossPromo' do |ss|
    ss.dependency 'PLSupport/Core'
    ss.source_files = 'CrossPromoKit/*.swift'
    ss.framework = 'AdSupport'
  end

  s.subspec 'Store' do |ss|
    ss.dependency 'PLSupport/Core'
    ss.dependency 'RMStore'
    ss.source_files = 'IAStoreHelper/*.swift'
    ss.exclude_files = 'IAStoreHelper/IAStoreHelper+ReceiptVerifier.swift'
  end

  s.subspec 'StoreVerification' do |ss|
    ss.dependency 'PLSupport/Store'
    ss.dependency 'RMStore/AppReceiptVerificator'
    ss.source_files = 'IAStoreHelper/IAStoreHelper+ReceiptVerifier.swift'
  end

  s.subspec 'LocalizedBundle' do |ss|
    ss.source_files = 'LocalizedBundle/*'
  end

  s.subspec 'KeyValueStore' do |ss|
    ss.source_files = 'KeyValueStore/*.swift'
  end

  s.subspec 'GameKit' do |ss|
    ss.dependency 'PLSupport/KeyValueStore'
    ss.framework = 'GameKit'
    ss.source_files = 'ReactiveGameKit/*.swift'
  end

  s.default_subspecs = 'Audio', 'KeyValueStore', 'Core'
end