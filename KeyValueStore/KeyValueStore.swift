//
//  KeyValueStore.swift
//  KeyValueStore
//
//  Created by Mihail Shulepov on 29/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

/// Decorator for KeyValueStoreEngine store with few conveince methods
open class KeyValueStore: NSObject, KeyValueStoreEngine {
    fileprivate let engine: KeyValueStoreEngine
    
    public init(engine: KeyValueStoreEngine) {
        self.engine =  engine
        super.init()
    }

    /// Integers
    open func setInteger(_ value: Int, forKey key: String) {
        self.engine.setInteger(value, forKey: key)
    }
    open func integerForKey(_ key: String, defaultValue: Int) -> Int {
        return self.engine.integerForKey(key) ?? defaultValue
    }
    open func integerForKey(_ key: String) -> Int? {
        return self.engine.integerForKey(key)
    }
       
    /// Floats
    open func setFloat(_ value: Float, forKey key: String) {
        self.engine.setFloat(value, forKey: key)
    }
    open func floatForKey(_ key: String) -> Float? {
        return self.engine.floatForKey(key)
    }
    open func floatForKey(_ key: String, defaultValue: Float) -> Float {
        return self.engine.floatForKey(key) ?? defaultValue
    }
    
    /// Booleans
    open func setBool(_ value: Bool, forKey key: String) {
        self.engine.setBool(value, forKey: key)
    }
    open func boolForKey(_ key: String) -> Bool? {
        return self.engine.boolForKey(key)
    }
    open func boolForKey(_ key: String, defaultValue: Bool) -> Bool {
        return self.engine.boolForKey(key) ?? defaultValue
    }
    
    /// Strings
    open func setString(_ value: String, forKey key: String) {
        self.engine.setString(value, forKey: key)
    }
    open func stringForKey(_ key: String) -> String? {
        return self.engine.stringForKey(key)
    }
    open func stringForKey(_ key: String, defaultValue: String) -> String {
        return self.engine.stringForKey(key) ?? defaultValue
    }
    
    /// Date
    open func setDate(_ value: Date, forKey key: String) {
        self.engine.setDate(value, forKey: key)
    }
    open func dateForKey(_ key: String) -> Date? {
        return self.engine.dateForKey(key)
    }
    open func dateForKey(_ key: String, defaultValue: Date) -> Date {
        return self.engine.dateForKey(key) ?? defaultValue
    }
    
    /// Data
    open func setData(_ value: Data, forKey key: String) {
        self.engine.setData(value, forKey: key)
    }
    open func dataForKey(_ key: String) -> Data? {
        return self.engine.dataForKey(key)
    }
    
    /// Removing
    open func removeObjectForKey(_ key: String) {
        self.engine.removeObjectForKey(key)
    }
    
    /// Custom objects
    open func setDictionary(_ dictionary: NSDictionary, forKey key: String) {
        setObject(dictionary, forKey: key)
    }
    
    open func setObject(_ object: NSCoding, forKey key: String) {
        let archive = NSKeyedArchiver.archivedData(withRootObject: object)
        setData(archive, forKey: key)
    }
    
    open func objectForKey<T>(_ key: String) -> T? {
        if let data = dataForKey(key) {
            if let unarchieved = NSKeyedUnarchiver.unarchiveObject(with: data) as? T {
                return unarchieved
            }
        }
        return nil
    }
    
    open func objectForKey<T>(_ key: String, defaultValue: T) -> T {
        if let data = dataForKey(key) {
            if let unarchieved = NSKeyedUnarchiver.unarchiveObject(with: data) as? T {
                return unarchieved
            }
        }
        return defaultValue
    }
   
    open func setArray(_ array: NSArray, forKey key: String) {
        setObject(array, forKey: key)
    }
    
    open func arrayForKey(_ key: String) -> NSArray {
        if let data = dataForKey(key) {
            if let unarchieved = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSArray {
                return unarchieved
            }
        }
        return NSArray()
    }
    
    open func addObjectToArray<T: AnyObject>(_ object: T, key: String) where T: NSCoding {
        let array: NSMutableArray = arrayForKey(key).mutableCopy() as! NSMutableArray
        array.add(object)
        setArray(array, forKey: key)
    }
           
    /// URLs
    open func setURL(_ value: URL, forKey key: String) {
        self.engine.setString(value.absoluteString, forKey: key)
    }
    open func urlForKey(_ key: String) -> URL? {
        if let path = self.engine.stringForKey(key) {
            return URL(string: path)
        }
        return nil
    }
    
    /// Saving
    open func save() {
        self.engine.save()
    }
}
