//
//  BannerHolderEmpty.swift
//  davinci
//
//  Created by Mihail Shulepov on 08/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

public class BannerHolder: UIView, IBannerHolder {
    @IBOutlet public weak var viewController: UIViewController!
    @IBInspectable public var bannerID: String = ""
    
    public override func intrinsicContentSize() -> CGSize {
       return CGSize(width: UIViewNoIntrinsicMetric, height: 0)
    }
}

private var sDefaultInterstitialScheduler: InterstitialScheduler?

public class InterstitialScheduler: NSObject, IInterstitialScheduler {
    @IBOutlet public weak var viewController: UIViewController?
    public var minIntervalBetweenAds = TimeUnit.seconds(60)
    @IBInspectable public var minSecondsBetweenAds: NSTimeInterval = 60
    @IBInspectable public var eventsCountToFire: Int = 1000
    @IBInspectable public var adUnitID: String = ""
    
    public init(adsSettings: AdsSettings = AdsSettings.defaultSettings, viewController: UIViewController? = nil) { }
    
    public func fireEvent() -> Bool {
        return false
    }
    
    public func showIfShould() { }
    
    public func forceShow() { }
}


public class RewardedVideoAds: NSObject, IRewardedVideoAds {
    public let location: String
    
    public init(location: String = "") {
        self.location = location
    }
    
    public var hasVideo = false
    public func showVideo(reward: (Int) -> Void) {}
    public func cacheVideo() {}
}