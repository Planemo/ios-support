//
//  RewardedVideo.swift
//  Mystery
//
//  Created by Mihail Shulepov on 03/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
#if !NO_MODULES
import ChartboostSDK
#endif

public class RewardedVideoAds: NSObject, IRewardedVideoAds {
    public let location: String
    private var rewardCallback: ((Int) -> Void)?
    
    public init(location: String = "Video") {
        self.location = location
        super.init()
        ChartboostMultiplexerDelegate.shared().addDelegate(self)
    }
    
    deinit {
        ChartboostMultiplexerDelegate.shared().removeDelegate(self)
    }
   
    public var hasVideo: Bool {
        return Chartboost.hasRewardedVideo(self.location)
    }
    
    public func showVideo(reward: (Int) -> Void) {
        if self.hasVideo {
            // keep self strongly until video not watched
            self.rewardCallback = { amount in
                reward(amount)
                self.rewardCallback = nil
            }
            Chartboost.showRewardedVideo(self.location)
        }
    }
    
    public func cacheVideo() {
        Chartboost.cacheRewardedVideo(self.location)
    }
}

extension RewardedVideoAds: ChartboostDelegate {
    public func didCacheRewardedVideo(location: String!) {
        NSLog("Did cache rewarded video for location: %@", location)
    }
    
    public func didFailToLoadRewardedVideo(location: String!, withError error: CBLoadError) {
        NSLog("Did fail to cache rewarded video due to: %@", "\(error.rawValue)")
    }
    
    public func didCompleteRewardedVideo(location: String!, withReward reward: Int32) {
        self.rewardCallback?(Int(reward))
    }
    
    public func didCloseRewardedVideo(location: String!) {
        self.rewardCallback?(0)
    }
}