//
//  IAProductHelper+UI.swift
//  test
//
//  Created by Mikhail Shulepov on 06.12.15.
//  Copyright © 2015 planemo. All rights reserved.
//

import Foundation
import StoreKit
#if !NO_MODULES
import MBProgressHUD
import ReactiveSwift
#endif

/// Helper class for buying IA products
open class IAProductBuyer {
    fileprivate var isHiding = false
    
    public init() {
        
    }
    
    open func purchase(_ productId: String, fromView view: UIView) -> SignalProducer<(), NSError> {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        let buying = IAStoreHelper.defaultHelper.productWithIdentifier(productId)
            .on(failed: { error in
                self.isHiding = true
                hud.hideWithErrorStatus(self.getLocalizedString("CouldNotLoadProduct"))
            })
            .flatMap(.merge) { product -> SignalProducer<(), NSError> in
                return IAStoreHelper.defaultHelper.purchaseProduct(product)
            }
        return keepHud(hud, whileExecuting: buying)
    }
    
    open func purchase(_ product: IAProduct, fromView view: UIView) -> SignalProducer<(), NSError> {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        let buying = IAStoreHelper.defaultHelper.purchaseProduct(product)
        return keepHud(hud, whileExecuting: buying)
    }
    
    fileprivate func keepHud(_ hud: MBProgressHUD, whileExecuting signalProducer: SignalProducer<(), NSError>) -> SignalProducer<(), NSError> {
        return signalProducer.on(failed: { error in
            if self.isHiding {
                //nothing to do, hud already being hiding
            } else if error.code == SKError.paymentCancelled.rawValue {
                hud.hide(animated: true)
            } else {
                let errorMsg = self.getLocalizedString("PurchaseFailed")
                hud.hideWithErrorStatus(errorMsg)
            }

        }, completed: {
            let message = self.getLocalizedString("PurchaseCompleted")
            hud.hideWithSuccessStatus(message)
        })
    }
    
    fileprivate func getLocalizedString(_ key: String) -> String {
        return IAStoreHelper.defaultHelper.getLocalizedString(key)
    }
}

open class IARestorer {
    public init() {
        
    }
    
    open func restoreInView(_ view: UIView) -> SignalProducer<[SKPaymentTransaction], NSError> {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = getLocalizedString("RestoringPurchases")
        return IAStoreHelper.defaultHelper.restoreTransactions().on(failed: { error in
            hud.hideWithErrorStatus(self.getLocalizedString("PurchasesRestoreFailed"))
        }, value: { transactions in
            if transactions.isEmpty {
                hud.hideWithInfoStatus(self.getLocalizedString("PurchasesNothingToRestore"))
            } else {
                hud.hideWithSuccessStatus(self.getLocalizedString("PurchasesRestored"))
            }
        })
    }
    
    fileprivate func getLocalizedString(_ key: String) -> String {
        return IAStoreHelper.defaultHelper.getLocalizedString(key)
    }
}
