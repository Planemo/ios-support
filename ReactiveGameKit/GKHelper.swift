//
//  GKHelper.swift
//  davinci
//
//  Created by Mihail Shulepov on 29/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import GameKit

#if !NO_MODULES
import ReactiveSwift
import enum Result.NoError
#endif

//TODO: take into account that player may be changed during application in background

open class GKHelper: NSObject {
    fileprivate let queue = DispatchQueue(label: "com.planemo.GKHelper", attributes: [])
   
    /// MARK: GK Configuration
    
    /// Full prefix including dot (for example 'com.planemo.squares.')
    /// So inside app you should simply use Completed5Levels instead of com.planemo.squares.Completed5Levels
    /// This also implies to leaderboards
    open var prefix = (Bundle.main.bundleIdentifier ?? "") + "."
    
    /// Whether game center should present default completion banners
    open var showAchievementsCompletionBanner = true

    /// To prevent cached data from modifying it should be encrypted
    fileprivate let cache: KeyValueStore?
    
    /// MARK: GK Public State
    
    /// GK Authenticated if error was occurred during authentification
    open let authenticated = MutableProperty(false)
    
    /// Contains only achievements for which progress > 0
    fileprivate(set) open var playerAchievements = [String: GKAchievement]()
    
    open let errorsSignal: Signal<NSError, NoError>
    fileprivate let errorsObserver: Signal<NSError, NoError>.Observer
    
    fileprivate let disposable = CompositeDisposable()
    
    public init(cache: KeyValueStoreEngine? = nil) {
        (self.errorsSignal, self.errorsObserver) = Signal<NSError, NoError>.pipe()
        
        if let cacheEngine = cache {
            self.cache = KeyValueStore(engine: cacheEngine)
        } else {
            self.cache = nil
        }
        super.init()
        self.setupAuthHandler()
        
        //autosave
        NotificationCenter.default
            .addObserver(self, selector: #selector(GKHelper.save), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    open func save() {
        self.cache?.save()
    }
}

private var sInstance: GKHelper!

public extension GKHelper {
    public class func startWithoutCache() {
        sInstance = GKHelper()
    }
    
    public class func startWithCache(_ cache: KeyValueStoreEngine) {
        sInstance = GKHelper(cache: cache)
    }
    
    public class func helper() -> GKHelper {
        return sInstance!
    }
}

private extension GKHelper {
    func onError(_ message: String, error: NSError!) {
        if error != nil {
            self.errorsObserver.send(value: error)
            NSLog("%@: %@", message, error.description)
        }
    }
}

/// MARK: Authentification

public extension GKHelper {
    fileprivate func setupAuthHandler() {
        let localPlayer = GKLocalPlayer.localPlayer()
        localPlayer.authenticateHandler = { [weak self] viewController, error in
            if self == nil {
                return
            }
            
            let strongSelf = self!
            if let viewController = viewController {
                strongSelf.authenticated.value = false
                if let topVC = UIApplication.shared.delegate?.window??.rootViewController {
                    topVC.present(viewController, animated: true, completion: nil)
                }
                
            } else if GKLocalPlayer.localPlayer().isAuthenticated {
                strongSelf.authenticated.value = true
                self?.onAuthStateChanged()
                
            } else {
                strongSelf.authenticated.value = false
                strongSelf.onError("Auth error", error: error as NSError!)
                self?.onAuthStateChanged()
            }
        }
    }
    
    fileprivate func onAuthStateChanged() {
        if self.authenticated.value {
            self.loadAchievementsProgress()
            
        } else {
            self.clearInMemoryUserRelativeData()
        }
    }
    
    fileprivate func clearInMemoryUserRelativeData() {
        //TODO:
    }
}


/// MARK: Achievements

public extension GKHelper {
    /// Request achievements loading
    /// Loading starts after user successfully logged in
    fileprivate func loadAchievementsProgress() {
        GKAchievement.loadAchievements { (achievements: [GKAchievement]?, error: Error?) in
            if error != nil {
                self.onError("Loading achievements error", error: error as! NSError)
            } else {
                achievements?.forEach { achievement in
                    let achieveName = achievement.identifier!.replacingOccurrences(of: self.prefix, with: "")
                    self.playerAchievements[achieveName] = achievement
                }
                
                self.reportCachedAchievements()
            }
        }
    }
    
    fileprivate var cachedAchievementsProgress: Dictionary<String, Double>? {
        get {
            return self.cache?.objectForKey("Achievements")
        }
        set(newValue) {
            if let value = newValue {
                self.cache?.setDictionary(value as NSDictionary, forKey: "Achievements")
            } else {
                self.cache?.removeObjectForKey("Achievements")
            }
        }
    }
    
    fileprivate func reportCachedAchievements() {
        if let dictionary = self.cachedAchievementsProgress {
            self.cachedAchievementsProgress = nil
            let names = Array(dictionary.keys)
            let progress = names.map{ dictionary[$0] ?? 0 }
            self.reportProgress(progress, forAchievementsWithNames: names)
        }
    }
    
    fileprivate func cacheProgress(_ progress: [Double], forAchievementsWithNames names: [String]) {
        var dictionary = self.cachedAchievementsProgress ?? [String : Double]()
        for (name, completionPercent) in zip(names, progress) {
            dictionary[name] = completionPercent
        }
        self.cachedAchievementsProgress = dictionary
    }
    
    fileprivate func achievementWithName(_ name: String) -> GKAchievement {
        if let achievement = self.playerAchievements[name] {
            return achievement
        } else {
            let identifier = self.prefix + name
            let achievement = GKAchievement(identifier: identifier)
            self.playerAchievements[name] = achievement
            return achievement
        }
    }
    
    ///
    /// Report progress for specified achievements (names without prefixes)
    ///
    public func reportProgress(_ progress: [Double], forAchievementsWithNames names: [String]) {
        let achievements = names.map { self.achievementWithName($0) }
        let updatedAchievements = (zip(progress, achievements))
            .filter { (newProgress, achievement) -> Bool in
                return achievement.percentComplete < newProgress - DBL_EPSILON
            }.map { (newProgress, achievement) -> GKAchievement in
                achievement.percentComplete = newProgress
                achievement.showsCompletionBanner = self.showAchievementsCompletionBanner
                return achievement
            }
        
        if GKLocalPlayer.localPlayer().isAuthenticated {
            if !updatedAchievements.isEmpty {
                GKAchievement.report(updatedAchievements, withCompletionHandler: { error in
                    if let error = error as? NSError {
                        NSLog("Report achievement error: %@", error.description)
                        self.cacheProgress(progress, forAchievementsWithNames: names)
                    }
                }) 
            }
        } else {
            self.cacheProgress(progress, forAchievementsWithNames: names)
        }
    }
}


/// MARK: Score and leaderboards

public extension GKHelper {
    fileprivate var cachedScoreData: [String: GKScore] {
        get {
            return self.cache?.objectForKey("Score") ?? [String: GKScore]()
        }
        set(newValue) {
            self.cache?.setDictionary(newValue as NSDictionary, forKey: "Score")
        }
    }
    
    fileprivate func bestCachedScoreForLeaderboard(_ leaderboardID: String) -> GKScore {
        return self.cachedScoreData[leaderboardID] ?? GKScore(leaderboardIdentifier: leaderboardID)
    }
  
    fileprivate func cacheScoreIfBest(_ score: GKScore) -> GKScore {
        let bestScore = bestCachedScoreForLeaderboard(score.leaderboardIdentifier)
        if score.value > bestScore.value {
            self.cachedScoreData[score.leaderboardIdentifier] = score
            return score
        }
        return bestScore
    }
    
    fileprivate func reportScore(_ score: Int64, forLeaderboardID leaderboardId: String, context: UInt64 = 0) {
        if score == 0 {
            return
        }
        let newScore = GKScore(leaderboardIdentifier: leaderboardId)
        newScore.value = score
        newScore.context = context
        let bestScore = cacheScoreIfBest(newScore)
        
        if !GKLocalPlayer.localPlayer().isAuthenticated {
            return
        }
        
        GKScore.report([bestScore], withCompletionHandler: { (error: Error?) in
            if let error = error as? NSError {
                NSLog("Score report error: %@", error.description)
            }
        })
    }
    
    ///
    /// Report score for speified leaderboard name (without prefix) in specified context
    ///
    public func reportScore(_ score: Int64, forLeaderboardName leaderboardName: String, context: UInt64 = 0) {
        let fullID = self.prefix + leaderboardName
        reportScore(score, forLeaderboardID: fullID, context: context)
    }
    
    ///
    /// Request best score for local player for specified leaderboard name (without prefix)
    /// GKScore posted in next event
    ///
    public func bestScoreForLeaderboard(_ leaderboardName: String) -> SignalProducer<GKScore, NoError> {
        let leaderboardID = self.prefix + leaderboardName
        return SignalProducer<GKScore, NoError> { observer, disposable in
            let bestCached = self.bestCachedScoreForLeaderboard(leaderboardID)
            let scoreRequest = GKLeaderboard()
            scoreRequest.identifier = leaderboardID
            scoreRequest.range = NSMakeRange(1, 1)
            scoreRequest.loadScores { (scores: [GKScore]?, error: Error?) in
                if let _ = error {
                    observer.send(value: bestCached)
                } else if let localPlayerScore = scoreRequest.localPlayerScore {
                    let best = localPlayerScore.value > bestCached.value ? localPlayerScore : bestCached
                    observer.send(value: best)
                } else {
                    observer.send(value: bestCached)
                }
                observer.sendCompleted()
            }
        }
    }
}


/// MARK: Default UI

extension GKHelper: GKGameCenterControllerDelegate {
    fileprivate func presentGKViewController(_ gameCenterVC: GKGameCenterViewController, fromViewController vc: UIViewController) {
         gameCenterVC.gameCenterDelegate = self
         vc.present(gameCenterVC, animated: true, completion: nil)
    }
    
    public func showLeaderboardWithName(_ leaderboardName: String!, fromViewController vc: UIViewController) {
        let leaderboardID = self.prefix + leaderboardName
        let gkVC = GKGameCenterViewController()
        gkVC.leaderboardIdentifier = leaderboardID
        gkVC.viewState = .leaderboards
        self.presentGKViewController(gkVC, fromViewController: vc)
    }
    
    public func showGameCenterViewController(fromViewController vc: UIViewController) {
        let gkVC = GKGameCenterViewController()
        gkVC.viewState = .default
        self.presentGKViewController(gkVC, fromViewController: vc)
    }
    
    public func showAchievementsViewController(fromViewController vc: UIViewController) {
        let gkVC = GKGameCenterViewController()
        gkVC.viewState = .achievements
        self.presentGKViewController(gkVC, fromViewController: vc)
    }
    
    public func showChallengesViewController(fromViewController vc: UIViewController) {
        let gkVC = GKGameCenterViewController()
        gkVC.viewState = .challenges
        self.presentGKViewController(gkVC, fromViewController: vc)
    }
    
    public func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
}


///
/// Extensions
///

public extension GKLeaderboard {
    public func loadScoresForLeaderboardName(_ leaderboardName: String) -> SignalProducer<[GKScore], NSError> {
        let leaderboardID = GKHelper.helper().prefix + leaderboardName
        self.identifier = leaderboardID
        return SignalProducer { observer, disposable in
            self.loadScores { (scores: [GKScore]?, error: Error?) in
                if let scores = scores {
                    observer.send(value: scores)
                    observer.sendCompleted()
                } else if let error = error {
                    observer.send(error: error as NSError)
                } else {
                    observer.sendCompleted()
                }
            }
        }
    }
}

public extension GKPlayer {
    public func loadPhotoForSize(_ size: GKPhotoSize) -> SignalProducer<UIImage, NSError> {
        return SignalProducer { observer, disposable in
            self.loadPhoto(forSize: size) { image, error in
                if let error = error {
                    observer.send(error: error as NSError)
                } else if let image = image {
                    observer.send(value: image)
                    observer.sendCompleted()
                } else {
                    observer.sendCompleted()
                }
            }
        }
    }
}
