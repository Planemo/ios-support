//
//  Bundle+Localization.swift
//  test
//
//  Created by Mikhail Shulepov on 29.10.15.
//  Copyright © 2015 planemo. All rights reserved.
//

import Foundation

private var BundleHandler: UInt8 = 0

class BundleEx: Bundle {
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        let simplifiedTableName: String?
        if let idx = tableName?.range(of: "~")?.lowerBound {
            simplifiedTableName = tableName?.substring(to: idx)
        } else {
            simplifiedTableName = tableName
        }
        
        if let bundle = objc_getAssociatedObject(self, &BundleHandler) as? Bundle {
            return bundle.localizedString(forKey: key, value: value, table: simplifiedTableName)
        } else {
            return super.localizedString(forKey: key, value: value, table: simplifiedTableName)
        }
    }
}

public extension Bundle {
    fileprivate static let SavedLanguageKey = "PLSavedLanguage"
    fileprivate static let LanguageDidChangeNotification = "PLLanguageDidChangeNotification"
    fileprivate static let NotificationLanguageKey = "PLLanguage"
    
    open override class func initialize() {
        struct Static {
            static var token: Int = 0
        }
        
        // make sure this isn't a subclass
        if self !== Bundle.self {
            return
        }
        
        object_setClass(Bundle.main, BundleEx.self)
        let oldLanguage = self.language
        self.language = oldLanguage
    }
    
    public class var language: String? {
        get {
            if let savedLanguage = UserDefaults.standard.object(forKey: SavedLanguageKey) as? String {
                return savedLanguage
            }
            //Check whether user prefers language other than "en" (as it's often a default language, but not native)
            let availableLanguages = self.availableLanguages
            let preferredLanguages = Locale.preferredLanguages.prefix(3)
            for preferredLanguage in preferredLanguages {
                let lowercase = preferredLanguage.lowercased()
                if !lowercase.contains("en") {
                    for availableLanguage in availableLanguages {
                        if lowercase.contains(availableLanguage) {
                            return availableLanguage
                        }
                    }
                }
            }
            if availableLanguages.contains("en") {
                return "en"
            }
            return availableLanguages.first
        }
        set {
            let bundle: Bundle?
            if let language = newValue {
                UserDefaults.standard.set(language, forKey: SavedLanguageKey)
                if let path = Bundle.main.path(forResource: language, ofType: "lproj") {
                    bundle = Bundle(path: path)
                } else {
                    bundle = nil
                }
            } else {
                UserDefaults.standard.removeObject(forKey: SavedLanguageKey)
                bundle = nil
            }

            objc_setAssociatedObject(Bundle.main, &BundleHandler, bundle, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            let info = [NotificationLanguageKey: newValue ?? ""]
            NotificationCenter.default.post(name: Notification.Name(rawValue: LanguageDidChangeNotification), object: nil, userInfo: info)
        }
    }
    
    public class var availableLanguages: [String] {
        let localizationBundles = Bundle.main.paths(forResourcesOfType: "lproj", inDirectory: nil)
        return localizationBundles.map { path -> String in
            let url = URL(string: path)!
            return url.deletingPathExtension().lastPathComponent
        }
    }
}
