//
//  IAProduct.swift
//  test
//
//  Created by Mikhail Shulepov on 27/03/15.
//  Copyright (c) 2015 planemo. All rights reserved.
//

import Foundation
import StoreKit
#if !NO_MODULES
import RMStore
import ReactiveSwift
#endif

public enum IAProductType {
    case consumable
    case nonConsumable
}

/// Description for an Item
open class IAProductDefinition {
    public typealias Action = (IAProductDefinition) -> Void
    
    open let type: IAProductType
    
    // Content Providing
    open var value: Int?
    open var provideContentAction: Action!
    open var removeContentAction: Action? //optional, only for non consumables
    open var contentProvided: (Void)->Bool = { false } // Return true for non-consumables if content was provided
    
    // Appearance customization
    open var icon: String?
    open var actionTitleLocKey = "Buy"
    
    public init(type: IAProductType, initializer: (IAProductDefinition)-> (Void)) {
        self.type = type
        initializer(self)
        assert(provideContentAction != nil, "action must be initialized")
    }
    
    open class func consumable(_ initializer: (IAProductDefinition)-> (Void)) -> IAProductDefinition {
        return IAProductDefinition(type: .consumable, initializer: initializer)
    }
    
    open class func nonConsumable(_ initializer: (IAProductDefinition)-> (Void)) -> IAProductDefinition {
        return IAProductDefinition(type: .nonConsumable, initializer: initializer)
    }
}

open class IAProduct {
    fileprivate let definition: IAProductDefinition
    internal let skProduct: SKProduct
    fileprivate let store: IAStoreHelper
    
    public init(definition: IAProductDefinition, skProduct: SKProduct, store: IAStoreHelper) {
        self.definition = definition
        self.skProduct = skProduct
        self.store = store
    }
    
    open var icon: UIImage? {
        if let name = self.definition.icon {
            return UIImage(named: name)
        }
        return nil
    }

    open var price: NSDecimalNumber {
        return self.skProduct.price
    }
    
    open var localizedPrice: String {
        return RMStore.localizedPrice(of: self.skProduct)
    }
    
    open var localizedActionTitle: String {
        return NSLocalizedString(self.definition.actionTitleLocKey, comment: "")
    }
    
    open var localizedTitle: String {
        return self.skProduct.localizedTitle
    }
    
    open var localizedDescription: String {
        return self.skProduct.localizedDescription
    }
    
    open var productIdentifier: String {
        return self.skProduct.productIdentifier
    }
    
    open var type: IAProductType {
        return self.definition.type
    }
    
    open var value: Int? {
        return self.definition.value
    }
    
    open var isPurchased: Bool {
        return self.definition.contentProvided()
    }
    
    open func purchase() -> SignalProducer<(), NSError> {
        return self.store.purchaseProduct(self)
    }
}
