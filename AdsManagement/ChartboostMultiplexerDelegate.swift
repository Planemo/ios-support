//
//  ChartboostMultiplexerDelegate.swift
//  FocusGames
//
//  Created by Mikhail Shulepov on 04/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

#if !NO_MODULES
//import Chartboost
#endif

private class WeakDelegate {
    weak var object: ChartboostDelegate?
    
    init(_ object: ChartboostDelegate) {
        self.object = object
    }
}

public class ChartboostMultiplexerDelegate: NSObject, ChartboostDelegate {
    private var delegates = [WeakDelegate]()
    private var strongDelegates: [ChartboostDelegate] {
        return self.delegates.map {
            return $0.object
        }.filter { $0 != nil }.map { $0! }
    }
    
    public class var shared: ChartboostMultiplexerDelegate {
        struct Static {
            static let Instance = ChartboostMultiplexerDelegate()
        }
        return Static.Instance
    }
    
    public func addDelegate(delegate: ChartboostDelegate) {
        let weakDelegate = WeakDelegate(delegate)
        self.delegates.append(weakDelegate)
    }
    
    public func removeDelegate(delegate: ChartboostDelegate) {
        self.delegates = self.delegates.filter { weakDelegate in
            return weakDelegate.object != nil && weakDelegate.object! !== delegate
        }
    }
    
    public func shouldRequestInterstitial(location: String!) -> Bool {
        return true
    }
    
    public func shouldDisplayInterstitial(location: String!) -> Bool {
        return true
    }
    
    public func didDisplayInterstitial(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didDisplayInterstitial?(location)
        }
    }
    
    public func didCacheInterstitial(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didCacheInterstitial?(location)
        }
    }
    
    public func didFailToLoadInterstitial(location: String!, withError error: CBLoadError) {
        for delegate in self.strongDelegates {
            delegate.didFailToLoadInterstitial?(location, withError: error)
        }
    }
    
    public func didFailToRecordClick(location: String!, withError error: CBClickError) {
        for delegate in self.strongDelegates {
            delegate.didFailToRecordClick?(location, withError: error)
        }
    }
    
    public func didDismissInterstitial(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didDismissInterstitial?(location)
        }
    }
    
    public func didCloseInterstitial(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didCloseInterstitial?(location)
        }
    }
    
    public func didClickInterstitial(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didClickInterstitial?(location)
        }
    }
    
    public func shouldDisplayMoreApps(location: String!) -> Bool {
        return true
    }
    
    public func didDisplayMoreApps(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didDisplayMoreApps?(location)
        }
    }
    
    public func didCacheMoreApps(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didCacheMoreApps?(location)
        }
    }
    
    public func didDismissMoreApps(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didDismissMoreApps?(location)
        }
    }
    
    public func didCloseMoreApps(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didCloseMoreApps?(location)
        }
    }
    
    public func didClickMoreApps(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didClickMoreApps?(location)
        }
    }
    
    public func didFailToLoadMoreApps(location: String!, withError error: CBLoadError) {
        for delegate in self.strongDelegates {
            delegate.didFailToLoadMoreApps?(location, withError: error)
        }
    }
    
    public func didPrefetchVideos() {
        for delegate in self.strongDelegates {
            delegate.didPrefetchVideos?()
        }
    }
    
    public func shouldDisplayRewardedVideo(location: String!) -> Bool {
        return true
    }
    
    public func didDisplayRewardedVideo(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didDisplayRewardedVideo?(location)
        }
    }
    
    public func didCacheRewardedVideo(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didCacheRewardedVideo?(location)
        }
    }
    
    public func didFailToLoadRewardedVideo(location: String!, withError error: CBLoadError) {
        for delegate in self.strongDelegates {
            delegate.didFailToLoadRewardedVideo?(location, withError: error)
        }
    }
    
    public func didDismissRewardedVideo(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didDismissRewardedVideo?(location)
        }
    }
    
    public func didCloseRewardedVideo(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didCloseRewardedVideo?(location)
        }
    }
    
    public func didClickRewardedVideo(location: String!) {
        for delegate in self.strongDelegates {
            delegate.didClickRewardedVideo?(location)
        }
    }
    
    public func didCompleteRewardedVideo(location: String!, withReward reward: Int32) {
        for delegate in self.strongDelegates {
            delegate.didCompleteRewardedVideo?(location, withReward: reward)
        }
    }
    
    public func didCacheInPlay(location: String!!) {
        for delegate in self.strongDelegates {
            delegate.didCacheInPlay?(location)
        }
    }
    
    public func didFailToLoadInPlay(location: String!, withError error: CBLoadError) {
        for delegate in self.strongDelegates {
            delegate.didFailToLoadInPlay?(location, withError: error)
        }
    }
    
    public func willDisplayVideo(location: String!) {
        for delegate in self.strongDelegates {
            delegate.willDisplayVideo?(location)
        }
    }
    
    public func didCompleteAppStoreSheetFlow() {
        for delegate in self.strongDelegates {
            delegate.didCompleteAppStoreSheetFlow?()
        }
    }
    
    public func didPauseClickForConfirmation() {
        for delegate in self.strongDelegates {
            delegate.didPauseClickForConfirmation?()
        }
    }
}