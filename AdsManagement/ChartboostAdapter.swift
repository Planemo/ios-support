//
//  ChartboostAdapter.swift
//  FocusGames
//
//  Created by Mikhail Shulepov on 04/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

#if !NO_MODULES
import GoogleMobileAds
//import Chartboost
#endif

@objc public class ChartboostAdapter: NSObject, GADCustomEventInterstitial, ChartboostDelegate {
    public weak var delegate: GADCustomEventInterstitialDelegate!
    
    public override init() {
        super.init()
        ChartboostMultiplexerDelegate.shared.addDelegate(self)
    }
    
    deinit {
        ChartboostMultiplexerDelegate.shared.removeDelegate(self)
    }
    
    private var location = CBLocationGameScreen
    
    public func requestInterstitialAdWithParameter(serverParameter: String!, label serverLabel: String!, request: GADCustomEventRequest!) {
        
        if let parameter = serverParameter {
            self.location = parameter
        }
        
        if Chartboost.hasInterstitial(self.location) {
            let when = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
            dispatch_after(when, dispatch_get_main_queue()) {
                self.didCacheInterstitial(self.location)
            }
        } else {
            Chartboost.cacheInterstitial(self.location)
        }
    }
    
    public func presentFromRootViewController(rootViewController: UIViewController!) {
        Chartboost.showInterstitial(self.location)
    }
    
    public func didCacheInterstitial(location: String!) {
        self.delegate.customEventInterstitialDidReceiveAd(self)
    }
    
    public func didFailToLoadInterstitial(location: String!, withError error: CBLoadError) {
        let nserror = NSError(domain: "com.planemo.ChartboostAdapter", code: Int(error.rawValue), userInfo: nil)
        self.delegate.customEventInterstitial(self, didFailAd: nserror)
    }
    
    public func didDismissInterstitial(location: String!) {
        self.delegate.customEventInterstitialDidDismiss(self)
    }
    
    public func didCloseInterstitial(location: String!) {
        
    }
    
    public func didClickInterstitial(location: String!) {
        self.delegate.customEventInterstitialWillLeaveApplication(self)
    }
    
}
