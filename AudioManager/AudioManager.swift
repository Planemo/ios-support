//
//  AudioManager.swift
//  davinci
//
//  Created by Mihail Shulepov on 06/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
import AVFoundation

open class AudioManager: NSObject {
    fileprivate enum UDKey: String {
        case MusicVolume = "MusicVolume"
        case SoundsVolume = "SoundsVolume"
    }
    
    fileprivate var musicPlayer: AVAudioPlayer?
    fileprivate var systemSounds = [String: SystemSound]()
    fileprivate var soundPlayers = [AVAudioPlayer]() //for retain purposes
    fileprivate let queue = DispatchQueue(label: "com.planemo.AudioManager", attributes: [])
    
    open class var defaultManager: AudioManager {
        struct Static {
            static var instance = AudioManager()
        }
        return Static.instance
    }
    
    public override init() {
        super.init()
    }
    
    open var musicVolume: Float {
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: UDKey.MusicVolume.rawValue)
            self.queue.async {
                self.musicPlayer?.volume = self.musicVolume; return
            }
        }
        get {
            return UserDefaults.standard.float(forKey: UDKey.MusicVolume.rawValue)
        }
    }
    
    open var soundsVolume: Float {
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: UDKey.SoundsVolume.rawValue)
        }
        get {
            return UserDefaults.standard.float(forKey: UDKey.SoundsVolume.rawValue)
        }
    }
    
    open var musicMuted: Bool {
        return musicVolume <= FLT_EPSILON
    }
    
    open var soundsMuted: Bool {
        return soundsVolume <= FLT_EPSILON
    }
    
    open func playMusic(_ filePath: String) {
        self.queue.async {
            let url = self.fullPathForResourceWithPath(filePath)
            do {
                self.musicPlayer = try AVAudioPlayer(contentsOf: url!)
            } catch let error {
                NSLog("Error creating player for music \(filePath): \(error)")
                self.musicPlayer = nil
                return
            }
            self.musicPlayer!.numberOfLoops = Int.max
            self.musicPlayer!.volume = self.musicVolume
            self.musicPlayer!.prepareToPlay()
            DispatchQueue.main.async {
                self.musicPlayer?.play()
            }
        }
    }
    
    open func pauseMusic() {
        musicPlayer?.pause()
    }
    
    open func resumeMusic() {
        musicPlayer?.play()
    }
    
    open func playSystemSound(_ filePath: String) {
        if soundsMuted {
            return
        }
        var sound: SystemSound?
        if let reuseSound = systemSounds[filePath] {
            sound = reuseSound
        } else {
            sound = SystemSound(file: self.fullPathForResourceWithPath(filePath))
            systemSounds[filePath] = sound
        }
        sound?.playAsSystemSound()
    }
    
    fileprivate func soundPlayersWithUrl(_ url: URL, playing: Bool?) -> [AVAudioPlayer] {
        if let playing = playing {
            return self.soundPlayers.filter { player in
                if player.isPlaying == playing {
                    if let playerUrl = player.url, playerUrl == url {
                        return true
                    }
                }
                return false
            }
        } else {
            return self.soundPlayers.filter { player in
                if let playerUrl = player.url, playerUrl == url {
                    return true
                }
                return false
            }
        }
    }
    
    fileprivate func createOrReuseSoundPlayerWithUrl(_ url: URL) -> AVAudioPlayer? {
        for player in self.soundPlayers where player.isPlaying == false {
            if let playerUrl = player.url, playerUrl == url {
               return player
            }
        }
        
        do {
            let player = try AVAudioPlayer(contentsOf: url)
            self.soundPlayers.append(player)
            return player
        } catch let error {
            NSLog("Error creating sound with file \(url.absoluteString): \(error)")
        }
        
        return nil
    }
    
    open func playSound(_ filePath: String) {
        if soundsMuted {
            return
        }
        self.queue.async {
            let fullURL = self.fullPathForResourceWithPath(filePath)
            if let player = self.createOrReuseSoundPlayerWithUrl(fullURL!) {
                player.volume = self.soundsVolume
                player.prepareToPlay()
                DispatchQueue.main.async {
                    player.play(); return
                }
            }
        }
    }
    
    open func stopSounds(_ filePath: String) {
        self.queue.async {
            let fullURL = self.fullPathForResourceWithPath(filePath)
            let players = self.soundPlayersWithUrl(fullURL!, playing: true)
            for player in players {
                player.stop()
            }
        }
    }
    
    fileprivate func fullPathForResourceWithPath(_ path: String) -> URL! {
        if let url = Bundle.main.url(forResource: path, withExtension: nil) {
            return url
        } else {
            return URL(fileURLWithPath: path)
        }
    }
}

private class SystemSound {
    fileprivate let soundID: SystemSoundID?
    init(file: URL) {
        var theSoundID: SystemSoundID = 0
        let error = AudioServicesCreateSystemSoundID(file as CFURL, &theSoundID)
        if error == kAudioServicesNoError {
            self.soundID = theSoundID
        } else {
            self.soundID = nil
        }
    }
    
    deinit {
        if let sound = soundID {
            AudioServicesDisposeSystemSoundID(sound)
        }
    }
    
    func playAsSystemSound() {
        if let sound = soundID {
            AudioServicesPlaySystemSound(sound)
        }
    }
    
    func playAsAlertSound() {
        if let sound = soundID {
            AudioServicesPlayAlertSound(sound)
        }
    }
}
