//
//  LifetimeTracker.swift
//  davinci
//
//  Created by Mihail Shulepov on 13/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
import UIKit.UIApplication

open class AppUsageTracker: NSObject {
    private struct UDKey {
        static let IsFirstLaunch = "PLAppIsFirstLaunch"
        static let LaunchesCount = "PLAppLaunchesCount"
        static let PrevLaunchVersion = "PLAppPrevLaunchVersion"
        static let LastSessionEndDate = "PLAppLastSessionEndDate"
    }
    open let isFirstLaunch: Bool
    open let launchesCount: Int
    open let isFirstLaunchAfterUpdate: Bool
    open var lastSessionEndTime: Date? {
        return UserDefaults.standard.object(forKey: UDKey.LastSessionEndDate) as? Date
    }
    
    /// Returns -1 if it is a first session
    open var timeSinceLastSession: TimeUnit {
        get {
            if let lastSessionDate = lastSessionEndTime {
                return TimeUnit.seconds(Date().timeIntervalSince(lastSessionDate))
            } else {
                return TimeUnit.seconds(-1)
            }
        }
    }
    
    open class var defaultTracker: AppUsageTracker {
        struct Static {
            static var instance: AppUsageTracker!
        }
        if Static.instance == nil {
            Static.instance = AppUsageTracker()
        }
        return Static.instance
    }
    
    private override init() {
        let ud = UserDefaults.standard
        isFirstLaunch = (ud.object(forKey: UDKey.IsFirstLaunch) == nil)
        ud.set(NSNumber(value: false as Bool), forKey: UDKey.IsFirstLaunch)
        
        launchesCount = ud.integer(forKey: UDKey.LaunchesCount) + 1
        ud.set(launchesCount, forKey: UDKey.LaunchesCount)

        let currentVersion = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
        if !isFirstLaunch {
            if let prevLaunchVersion = ud.object(forKey: UDKey.PrevLaunchVersion) as? String {
                isFirstLaunchAfterUpdate = currentVersion != prevLaunchVersion
            } else {
                isFirstLaunchAfterUpdate = false
            }
        } else {
            isFirstLaunchAfterUpdate = false
        }
        ud.set(currentVersion, forKey: UDKey.PrevLaunchVersion)
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppUsageTracker.applicationWillResignActive),
            name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func applicationWillResignActive() {
        storeTimeForKey(UDKey.LastSessionEndDate)
    }
    
    open func storeTimeForKey(_ key: String) {
        UserDefaults.standard.set(Date(), forKey: key)
    }
    
    open func timePassedSinceLastStoreForKey(_ key: String) -> TimeUnit? {
        if let storedDate = UserDefaults.standard.object(forKey: key) as? Date {
            return Date().timeSinceDate(storedDate)
        }
        return nil
    }
}
