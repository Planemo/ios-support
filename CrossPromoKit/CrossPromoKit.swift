//
//  CrossPromoItem.swift
//  davinci
//
//  Created by Mihail Shulepov on 16/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
import AdSupport
import StoreKit
#if !NO_MODULES
import MBProgressHUD
#endif

public let CrossPromoGroupMenu = "Menu"
public let CrossPromoGroupShop = "Shop"
public let CrossPromoGroupDialog = "Dialog"
public let CrossPromoGroupDefault = "Default"

open class CrossPromoKit: NSObject, SKStoreProductViewControllerDelegate {
    open class Filter {
        open let groups: [String]
        open let languages: [String]
        
        public init(groups: [String], languages: [String] = []) {
            self.languages = languages
            self.groups = groups
        }
        
        open func fitTo(_ filter: Filter) -> Bool {
            for group in filter.groups {
                if !self.groups.contains(group) {
                    return false
                }
            }
            if !self.languages.isEmpty {
                for language in filter.languages {
                    if !self.languages.contains(language) {
                        return false
                    }
                }
            }
            return true
        }
    }
    
    fileprivate class ItemEntry {
        let filter: Filter
        let item: CrossPromoItem
        
        init(item: CrossPromoItem, filter: Filter) {
            self.filter = filter
            self.item = item
        }
    }
    
    fileprivate var items = [ItemEntry]()
    fileprivate var allItems: [CrossPromoItem] {
        return items.map { entry in return entry.item }
    }
    open var rewardAction: ((Int) -> Void)!
    open var affiliateToken = "10lJDF"
    open var campaign = Bundle.main.bundleIdentifier ?? "ios_cross_promo"
    open var autoRewarding = true
    
    // used for tracking successfull installations
    open var gaPropertyID: String?
    
    open static let defaultKit = CrossPromoKit()
    
    open func openAppStoreForApp(_ applicationID: String, fromViewController sourceVC: UIViewController) {
        if let gaTrackingURL = self.buildGATrackingURL(applicationID) {
            let request = URLRequest(url: gaTrackingURL)
            NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { response, data, error in

            }
        }
        let hud = MBProgressHUD.showAdded(to: sourceVC.view, animated: true)
        let storeVC = SKStoreProductViewController()
        storeVC.delegate = self

        var params: [String: AnyObject]!
        switch UIDevice.current.systemVersion.compare("8.0.0", options: NSString.CompareOptions.numeric) {
        case .orderedSame, .orderedDescending:
            params = [SKStoreProductParameterITunesItemIdentifier: applicationID as AnyObject,
                SKStoreProductParameterCampaignToken: self.campaign as AnyObject,
                SKStoreProductParameterAffiliateToken: self.affiliateToken as AnyObject]

        case .orderedAscending:
            params = [SKStoreProductParameterITunesItemIdentifier: applicationID as AnyObject]
        }
       
        storeVC.loadProduct(withParameters: params, completionBlock: { (completed, error) in
            if completed && error == nil {
                hud.hide(animated: true)
                sourceVC.present(storeVC, animated: true, completion: nil)
            } else {
                hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
            }
        })
    }
    
    fileprivate func buildGATrackingURL(_ appID: String) -> URL? {
        if let propertyID = self.gaPropertyID {
            if !ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
                return nil
            }
            let cm = "ios_\(self.campaign)"
            let idfa = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            return URL(string: "https://click.google-analytics.com/ping?tid=\(propertyID)&aid=\(appID)&idfa=\(idfa)&cs=cross_promo&cm=\(cm)&hash=md5")
        }
        return nil
    }
    
    open class func isInstalledAppWithScheme(_ scheme: String) -> Bool {
        if let url = URL(string: "\(scheme)://") {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override init() {
        super.init()
        NotificationCenter.default.addObserver(self,
            selector: #selector(CrossPromoKit.makeAutoRewardsIfShould),
            name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    open func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    open func registerItem(_ item: CrossPromoItem, filter: Filter) {
        let entry = ItemEntry(item: item, filter: filter)
        self.items.append(entry)
    }
    
    open func itemsForFilter(_ filter: Filter) -> [CrossPromoItem] {
        return self.items.filter { entry -> Bool in
            return entry.filter.fitTo(filter)
        }.map { entry in
            return entry.item
        }
    }
    
    func makeAutoRewardsIfShould() {
        if self.autoRewarding {
            self.makeRewardsIfShould()
        }
    }
    
    open func makeRewardsIfShould() {
        let newlyInstalled = self.allItems
            .filter { !$0.isRewarded }
            .filter { $0.isInstalled }

        let totalReward = newlyInstalled.reduce(0) { acc, item -> Int in
            return acc + item.rewardAmount
        }
        rewardAction(totalReward)
        for item in newlyInstalled {
            item.isRewarded = true
        }
    }
}

public extension CrossPromoKit {
    public func handlePushNotification() -> Bool {
        return false
    }
}

open class AppStoreOpener: NSObject {
    @IBOutlet weak var viewController: UIViewController!
    
    @IBAction
    open func openStore(_ view: UIView) {
        if let vc = self.viewController {
            CrossPromoKit.defaultKit.openAppStoreForApp("\(view.tag)", fromViewController: vc)
        }
    }
}

open class CrossPromoItem: NSObject {
    open var nameKey = ""
    open var aboutKey = ""
    
    open var applicationID: UInt
    open var iconName: String?
    open var schemes: [String]?
    open var rewardAmount: Int = 0
    
    public init(applicationID: UInt) {
        self.applicationID = applicationID
        super.init()
    }
    
    public convenience init(applicationID: UInt,
        nameKey: String,  aboutKey: String = "",
        iconName: String? = nil, schemes: [String]? = nil, rewardAmount: Int = 0) {
            self.init(applicationID: applicationID)
            self.nameKey = nameKey
            self.aboutKey = aboutKey
            self.iconName = iconName
            self.schemes = schemes
            self.rewardAmount = rewardAmount
    }
    
    open var isInstalled: Bool {
        if let appSchemes = schemes {
            for scheme in appSchemes {
                if CrossPromoKit.isInstalledAppWithScheme(scheme) {
                    return true
                }
            }
        }
        return false
    }
    
    open var localizedName: String {
        return NSLocalizedString(nameKey, comment: "Short Name")
    }
    
    open var localizedAbout: String {
        return NSLocalizedString(aboutKey, comment: "About")
    }
    
    open var icon: UIImage? {
        if let iconFileName = iconName {
            if let image = UIImage(named: iconFileName) {
                return image
            }
            let documentsDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let imagePath = documentsDir.appendingPathComponent(iconFileName)
            return UIImage(contentsOfFile: imagePath.absoluteString)
        }
        return nil
    }
    
    open var isRewarded: Bool {
        get {
            if let appScheme = self.schemes?.first {
                return UserDefaults.standard.bool(forKey: "PLRewardedFor\(appScheme)")
            }
            return true
        }
        set(newValue) {
            if let appScheme = self.schemes?.first {
                UserDefaults.standard.set(newValue, forKey: "PLRewardedFor\(appScheme)")
            }
            NSLog("Reward for app: %@ / %ud", self.localizedName, self.applicationID)
        }
    }
}

// MARK: Equatable
public func ==(lhs: CrossPromoItem, rhs: CrossPromoItem) -> Bool {
    return lhs.applicationID == rhs.applicationID
}
