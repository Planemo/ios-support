//
//  BannerHolder.swift
//  davinci
//
//  Created by Mihail Shulepov on 07/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
#if !NO_MODULES
import GoogleMobileAds
import ReactiveCocoa
#endif

public class BannerHolder: UIView, IBannerHolder {
    private var bannerView: GADBannerView?
    
    private var mBannerID: String?
    @IBInspectable public var bannerID: String {
        get { return mBannerID ?? adsSettings.defaultBannerAdUnitID }
        set(newValue) { mBannerID = newValue }
    }
    @IBOutlet public weak var viewController: UIViewController!
    
    private var isReady: Bool = false {
        didSet {
            self.invalidateIntrinsicContentSizeAnimated(isReady)
        }
    }
    
    public override var hidden: Bool {
        didSet {
            self.invalidateIntrinsicContentSizeAnimated(true)
        }
    }
    
    public var adsSettings = AdsSettings.defaultSettings {
        didSet {
            //TODO:
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    public override func intrinsicContentSize() -> CGSize {
        if self.bannerView != nil && self.isReady && !self.hidden {
            return CGSize(width: UIViewNoIntrinsicMetric, height: self.bannerView!.bounds.size.height)
        } else {
            return CGSize(width: UIViewNoIntrinsicMetric, height: 0)
        }
    }
    
    private func invalidateIntrinsicContentSizeAnimated(animated: Bool) {
        let block  = { () -> Void in
            self.invalidateIntrinsicContentSize()
            self.superview?.layoutIfNeeded()
        }
        if animated {
            UIView.animateWithDuration(0.3, animations: block)
        }  else {
            block()
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChanged:",
            name: UIApplicationWillChangeStatusBarOrientationNotification, object: nil)
        
        self.setupWithNewSettings()
    }
    
    private func setupWithNewSettings() {
        self.adsSettings.adsEnabledNow.producer.start(next: { enabled in
            self.refresh()
        })
    }
    
    internal func onOrientationChanged(notification: NSNotification) {
        let orientationCode = notification.userInfo?[UIApplicationStatusBarOrientationUserInfoKey] as? NSNumber
        if let orientation = UIInterfaceOrientation(rawValue: orientationCode?.integerValue ?? 0) {
            self.refreshForInterfaceOrientation(orientation)
        }
    }
    
    private func refresh() {
        self.refreshForInterfaceOrientation(UIApplication.sharedApplication().statusBarOrientation)
    }
    
    private func refreshForInterfaceOrientation(orientation: UIInterfaceOrientation) {
        self.isReady = false
        self.bannerView?.delegate = nil
        self.bannerView?.removeFromSuperview()
        self.bannerView = nil
        
        if self.adsSettings.adsDisabledNow.value {
            return
        }
        var newBanner: GADBannerView!
        if UIInterfaceOrientationIsPortrait(orientation) {
            newBanner = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        } else {
            newBanner = GADBannerView(adSize: kGADAdSizeSmartBannerLandscape)
        }
        newBanner.adUnitID = self.bannerID
        newBanner.delegate = self
        newBanner.rootViewController = self.viewController
        self.bannerView = newBanner
        self.addSubview(newBanner)
        let request = GADRequest()
        newBanner.loadRequest(request)
    }
}

extension BannerHolder: GADBannerViewDelegate {
    public func adViewDidReceiveAd(view: GADBannerView) {
        self.isReady = true
    }
    
    public func adView(view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        self.isReady = false
        delay(TimeUnit.seconds(10)) { [weak self] in
            if let strongSelf = self {
                if !strongSelf.isReady {
                    strongSelf.refresh()
                }
            }
        }
    }
}