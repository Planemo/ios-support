//
//  PlaySoundTarget.swift
//  test
//
//  Created by Mikhail Shulepov on 10/04/15.
//  Copyright (c) 2015 planemo. All rights reserved.
//

import Foundation

open class PlaySoundTarget: NSObject {
    @IBInspectable open var soundName: String?
    
    @IBAction open func playSound() {
        if let soundName = self.soundName {
            AudioManager.defaultManager.playSound(soundName)
        } else {
            NSLog("Sound name for PlaySoundTarget not specified")
        }
    }
}
