//
//  TimeUnit.swift
//  davinci
//
//  Created by Mihail Shulepov on 20/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

public enum TimeUnitType : Int32 {
    case milliSecond
    case second
    case minute
    case hour
    case day
    case week
}

public struct TimeUnit {
    public let value: Double
    public let type: TimeUnitType
    
    public static func milliSeconds(_ value: Double) -> TimeUnit {
        return TimeUnit(value: value, type: .milliSecond)
    }
    
    public static func seconds(_ value: Double) -> TimeUnit {
        return TimeUnit(value: value, type: .second)
    }
    
    public static func minutes(_ value: Double) -> TimeUnit {
        return TimeUnit(value: value, type: .minute)
    }
    
    public static func hours(_ value: Double) -> TimeUnit {
        return TimeUnit(value: value, type: .hour)
    }
    
    public static func days(_ value: Double) -> TimeUnit {
        return TimeUnit(value: value, type: .day)
    }
    
    public static func weeks(_ value: Double) -> TimeUnit {
        return TimeUnit(value: value, type: .week)
    }
    
    public init(value: Double, type: TimeUnitType) {
        self.value = value
        self.type = type
    }
    
    static func secondsForType(_ type: TimeUnitType) -> Double {
        switch type {
        case .milliSecond: return 0.001
        case .second: return 1
        case .minute: return 60
        case .hour: return 3600
        case .day: return 86400
        case .week: return 604800
        }
    }
    
    static func nameForType(_ type: TimeUnitType) -> String {
        switch type {
        case .milliSecond: return "MilliSecond"
        case .second: return "Second"
        case .minute: return "Minute"
        case .hour: return "Hour"
        case .day: return "Day"
        case .week: return "Week"
        }
    }
    
    public var seconds: TimeInterval {
        return self.value * TimeUnit.secondsForType(self.type)
    }
}

public extension TimeUnit {
    public init(from: TimeUnit, type targetType: TimeUnitType) {
        let fromTypeSeconds = TimeUnit.secondsForType(from.type)
        let targetTypeSeconds = TimeUnit.secondsForType(targetType)
        let convertion = targetTypeSeconds / fromTypeSeconds
        let convertedValue = from.value / convertion
        self.init(value: convertedValue, type: targetType)
    }
    
    public func convertTo(type targetType: TimeUnitType) -> TimeUnit {
        return TimeUnit(from: self, type: targetType)
    }
}

extension TimeUnit: CustomStringConvertible, CustomDebugStringConvertible {
    public var description: String {
        return toString()
    }
    
    public var debugDescription: String {
        return self.description
    }
}

public extension TimeUnit {
    public init(string: String) {
        let components = string.components(separatedBy: CharacterSet.whitespaces)
        let value = strtod(components.first!, nil)
        let type = TimeUnit.typeForName(components.last)
        self.init(value: value, type: type)
    }
    
    public func toString() -> String {
        return "\(self.value) \(TimeUnit.nameForType(self.type))"
    }
    
    fileprivate static func typeForName(_ name: String?) -> TimeUnitType {
        if let name = name {
            let namesMap: [(String, TimeUnitType)] = [("millisecond", .milliSecond), ("second", .second),
                ("minute", .minute), ("hour", .hour),
                ("day", .day), ("week", .week)
            ]
            for nameType in namesMap {
                if name.range(of: nameType.0, options: .caseInsensitive) != nil {
                    return nameType.1
                }
            }
        }
        
        return .second
    }
}

extension TimeUnit: Equatable, Hashable {
    public var hashValue: Int {
        return Int((self.value * 1000).truncatingRemainder(dividingBy: Double(Int.max)))
    }
}

public func ==(lhs: TimeUnit, rhs: TimeUnit) -> Bool {
    return abs(lhs.seconds - rhs.seconds) < 0.001
}

extension TimeUnit: Comparable {
    
}

public func <=(lhs: TimeUnit, rhs: TimeUnit) -> Bool {
    return !(lhs > rhs)
}

public func >=(lhs: TimeUnit, rhs: TimeUnit) -> Bool {
    return !(lhs < rhs)
}

public func >(lhs: TimeUnit, rhs: TimeUnit) -> Bool {
    return lhs.seconds > rhs.seconds
}

public func <(lhs: TimeUnit, rhs: TimeUnit) -> Bool {
    return lhs.seconds < rhs.seconds
}

public func +(lhs: TimeUnit, rhs: TimeUnit) -> TimeUnit {
    return TimeUnit.seconds(lhs.seconds + rhs.seconds)
}

public func -(lhs: TimeUnit, rhs: TimeUnit) -> TimeUnit {
    return TimeUnit.seconds(lhs.seconds - rhs.seconds)
}

public func +(lhs: TimeUnit, rhs: Double) -> TimeUnit {
    return TimeUnit(value: lhs.value + rhs, type: lhs.type)
}

public func -(lhs: TimeUnit, rhs: Double) -> TimeUnit {
    return TimeUnit(value: lhs.value - rhs, type: lhs.type)
}

public func *(lhs: TimeUnit, rhs: TimeUnit) -> TimeUnit {
    return TimeUnit.seconds(lhs.seconds * rhs.seconds)
}

public func /(lhs: TimeUnit, rhs: TimeUnit) -> TimeUnit {
    return TimeUnit.seconds(lhs.seconds / rhs.seconds)
}

public func *(lhs: TimeUnit, rhs: Double) -> TimeUnit {
    return TimeUnit(value: lhs.value * rhs, type: lhs.type)
}

public func /(lhs: TimeUnit, rhs: Double) -> TimeUnit {
    return TimeUnit(value: lhs.value / rhs, type: lhs.type)
}

public func delay(_ time: TimeUnit, queue: DispatchQueue, block: @escaping ()->()) {
    let when = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * time.seconds)) / Double(NSEC_PER_SEC)
    queue.asyncAfter(deadline: when, execute: block)
}

public func delay(_ time: TimeUnit, block: @escaping ()->()) {
    delay(time, queue: DispatchQueue.main, block: block)
}

public extension Date {
    public func timeSinceDate(_ date: Date) -> TimeUnit {
        return TimeUnit.seconds(self.timeIntervalSince(date))
    }
}
