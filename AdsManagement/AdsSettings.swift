//
//  AdsScheduler.swift
//  davinci
//
//  Created by Mihail Shulepov on 07/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
#if !NO_MODULES
import ReactiveSwift
import enum Result.NoError
#endif

// TODO: Possibly should be added separate options for banner ads and interstitial ads / names
// For example, to disable only banners in shop
open class AdsSettings: NSObject {
    private enum UDKey: String {
        case UsesCount = "AdsSettings.UsesCount"
        case DisablingEndDate = "AdsSettings.DisablingEndDate"
        case AdsDisabled = "AdsSettings.Disabled"
        case EventsCount = "AdsSettings.EventsCount"
    }
    
    private let usesCount: PersistentProperty<Int>
    private let eventsCount: PersistentProperty<Int>
    open let appAdsDisabled: PersistentProperty<Bool>
    open let disabledThisSession = MutableProperty(false)
    open let disabledDueDate: OptionalPersistentProperty<Date>
    
    open var defaultInterstitialAdUnitID = ""
    open var defaultBannerAdUnitID = ""
    
    /// Ads will not be shown while usesCount < minUsesCount
    open var minUsesCount = 0
    
    /// Ads will not be shown while eventsCount < minEventsCount
    open var minEventsCount = 0

    public init(storage: KeyValueStore) {
        self.usesCount = PersistentProperty<Int>.int(storage: storage, key: UDKey.UsesCount.rawValue, defaultValue: 0)
        self.eventsCount = PersistentProperty<Int>.int(storage: storage, key: UDKey.EventsCount.rawValue, defaultValue: 0)
        self.appAdsDisabled = PersistentProperty<Bool>.bool(storage: storage, key: UDKey.AdsDisabled.rawValue, defaultValue: false)
        self.disabledDueDate = OptionalPersistentProperty<Date>.date(storage: storage, key: UDKey.DisablingEndDate.rawValue)
        
        self._adsDisabledNow = MutableProperty(false)
        self._adsEnabledNow = MutableProperty(true)
        self.adsDisabledNow = Property(self._adsDisabledNow)
        self.adsEnabledNow = Property(self._adsEnabledNow)
            
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(AdsSettings.incrementUsesCount),
            name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        let notEnoughEventsCount = self.eventsCount.producer.map { [unowned self] count -> Bool in count < self.minEventsCount }
        let notEnoughUsesCount = self.usesCount.producer.map { [unowned self] count -> Bool in count < self.minUsesCount }
        let disabledThisSession = self.disabledThisSession.producer
        let appDisabled = self.appAdsDisabled.producer
        let disabledDueDate = self.disabledDueDate.producer.flatMap(.latest) { (date) -> SignalProducer<Bool, NoError> in
            if let date = date, NSDate().compare(date) == .orderedAscending {
                return SignalProducer { observer, compositeDisposable in
                    observer.send(value: true)
                    let scheduler = QueueScheduler.main
                    let disposable = scheduler.schedule(after: date) {
                        observer.send(value: false)
                        observer.sendCompleted()
                    }
                    if let disposable = disposable {
                        compositeDisposable.add(disposable)
                    }
                }
            }
            return SignalProducer(value: false)
        }
        
        let combinedProducer = SignalProducer.combineLatest(notEnoughEventsCount, notEnoughUsesCount, disabledThisSession, appDisabled, disabledDueDate)
        self._adsDisabledNow <~ combinedProducer.map { d1, d2, d3, d4, d5 -> Bool in
            return d1 || d2 || d3 || d4 || d5
        }.skipRepeats()
        
        
        self._adsEnabledNow <~ self._adsDisabledNow.producer.map { !$0 }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    internal dynamic func incrementUsesCount() {
        self.usesCount.value += 1
    }
    
    private let _adsEnabledNow: MutableProperty<Bool>
    private let _adsDisabledNow: MutableProperty<Bool>
    
    open let adsEnabledNow: Property<Bool>
    open let adsDisabledNow: Property<Bool>
    
    open class var defaultSettings: AdsSettings {
        struct Static {
            static var Instance: AdsSettings?
        }
        if Static.Instance == nil {
            let udEngine = UserDefaultsStore(userDefaults: UserDefaults.standard)
            Static.Instance = AdsSettings(storage: KeyValueStore(engine: udEngine))
        }
        return Static.Instance!
    }
    
    /// Encrement events count by 1
    /// When events rich minEventsCount ads will enabled (if other requirements are met)
    open func incrementEventsCount() {
        self.eventsCount.value += 1
    }
    
    /// Disable ads just for this session (until app will be terminated)
    open func disableAdsForThisSession() {
        self.disabledThisSession.value = true
    }
    
    /// E.g. disable ads for a day after some user action (share, purchase)..
    /// Disable ads for first minute after launch
    /// ...
    open func disableAdsUntilDate(_ date: Date) {
        self.disabledDueDate.value = date
    }
}
