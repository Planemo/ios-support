//
//  InterstitialScheduler.swift
//  davinci
//
//  Created by Mihail Shulepov on 07/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
#if !NO_MODULES
import GoogleMobileAds
#endif

public class InterstitialScheduler: NSObject, IInterstitialScheduler {
    @IBOutlet public weak var viewController: UIViewController?

    private var interstitial: GADInterstitial!
    private let settings: AdsSettings
    
    private var eventsCount: Int = 0 {
        didSet {
            showIfShould()
        }
    }
    private var lastTimeAdsShown: NSDate?

    /// How often ads may be displayed (in seconds)
    public var minIntervalBetweenAds = TimeUnit.seconds(60)
    
    @IBInspectable public var minSecondsBetweenAds: NSTimeInterval {
        get { return self.minIntervalBetweenAds.seconds }
        set { self.minIntervalBetweenAds = TimeUnit.seconds(newValue) }
    }
    
    /// When ads should fire (for example, fire every 5 loses)
    @IBInspectable public var eventsCountToFire = 5
    
    private var mAdUnitID: String?
    @IBInspectable public var adUnitID: String {
        get { return mAdUnitID ?? settings.defaultInterstitialAdUnitID }
        set { mAdUnitID = newValue }
    }
    
    private var showWhenReady = false
    
    public init(adsSettings: AdsSettings = AdsSettings.defaultSettings, viewController: UIViewController? = nil) {
        self.settings = adsSettings
        self.viewController = viewController
        super.init()
    }
    
    public func fireEvent() -> Bool {
        self.eventsCount += 1
        return self.shouldShowAds
    }
}

public extension InterstitialScheduler {
    private var timeSinceLastAds: TimeUnit {
        let seconds = self.lastTimeAdsShown == nil
            ? minIntervalBetweenAds.seconds + 1
            : NSDate().timeIntervalSinceDate(lastTimeAdsShown!)
        return TimeUnit.seconds(seconds)
    }
    
    private var interstitialReady: Bool {
        return interstitial == nil ? false : interstitial.isReady
    }
    
    private var shouldShowAds: Bool {
        return settings.adsEnabledNow.value
            && interstitialReady
            && eventsCount >= eventsCountToFire
            && timeSinceLastAds > minIntervalBetweenAds
    }
    
    public func showIfShould() {
        if shouldShowAds {
            self.forceShow()
        } else {
            self.cacheIfAdsEnabled()
        }
    }
    
    public func forceShow() {
        self.lastTimeAdsShown = NSDate()
        self.eventsCount = 0

        if let vc = self.viewController {
            if self.interstitial != nil && self.interstitial.isReady {
                self.interstitial.presentFromRootViewController(vc)
                self.interstitial = nil
                self.cache()
            } else {
                showWhenReady = true
                self.cacheIfAdsEnabled()
            }
        } else {
            NSLog("WARNING: Interstitial scheduler - ViewController not set")
        }
    }
}

public extension InterstitialScheduler {
    private func cacheIfAdsEnabled() {
        if settings.adsEnabledNow.value {
            cache()
        }
    }
    
    private func uncache() {
        self.interstitial?.delegate = nil
        self.interstitial = nil
    }
    
    private func cache() {
        if self.interstitial == nil {
            self.interstitial = GADInterstitial(adUnitID: self.adUnitID)
            interstitial.delegate = self
            let request = GADRequest()
            interstitial.loadRequest(request)
        }
    }
}

extension InterstitialScheduler: GADInterstitialDelegate {
    public func interstitialDidReceiveAd(ad: GADInterstitial!) {
        if showWhenReady {
            showWhenReady = false
            forceShow()
        }
    }
    
    public func interstitial(ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        delay(TimeUnit.seconds(20)) { [weak self] in
            self?.cacheIfAdsEnabled(); return
        }
    }
    
    public func interstitialDidDismissScreen(ad: GADInterstitial!) {
        ad.delegate = nil
    }
}